cat new_data/treebanks/raw | python tagger/tag_raw_file.py | python heb_to_tb.py | python bin/fix_dep_tb.py | python bin/convert_morph_field.py |python post_tagger_changes.py > raw.tagged
python bin/retokenize_deptb.py raw.tagged new_data/treebanks/raw | python tagged_to_lattice.py > new_data/treebanks/raw.tagged.retoked

python lattices/code/makelattice.py < new_data/treebanks/raw | python post_lattice_changes.py > new_data/treebanks/raw.lattice
