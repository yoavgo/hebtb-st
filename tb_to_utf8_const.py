import sys
sys.path.append("lattices/code")
import translate
sys.path.append("bin")
import hebtb_reader
for i,tree in hebtb_reader.read_one_tree_per_line_as_stream(sys.stdin):
   for l in tree.collect_leaves():
      w = l.get_word()
      if w[0] != 'y': l.set_word(translate.tb2heb(w))
   print tree.as_sexpr().encode("utf8")
