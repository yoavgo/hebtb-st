import sys
i = 0
for line in sys.stdin:
   line = line.strip().split()
   if not line:
      print
      i = 0
      continue
   id, form, lemma, cpos, fpos, feats, par, prel, x, y = line
   print "\t".join([str(i),str(i+1),form,lemma,cpos,fpos,feats,id.split(".")[0]])
   i = i + 1


