python ../bin/fix_layered_tb.py < tbv2.trees.kcpos.layered > tbv2.trees.kcpos.layered.fixed

python ../bin/fix_dep_tb.py < hebdeptb.seg > deps.seg.with_awt_under_s-anp

cat deps.seg.with_awt_under_s-anp > treebanks/deptb
cat tbv2.trees.kcpos.layered.fixed |python ../bin/skip_lines.py > treebanks/consttb.layered

# verify that yields match:
cat treebanks/deptb           |python ../bin/get_yield_from_conll.py |python ../bin/dep_remove_suff.py > deps
cat treebanks/consttb.layered |python ../bin/get_yields.py > layered
diff deps layered

# unify the tags (I didn't actually run it from this dir)
python ../align_pos_tags/align_pos_tags.py # this will generate treebanks/*.unified_pos

# fix the morph features for the !!UNK!! and !!MISS!! tokens, and change the morph representation
cat treebanks/deptb.unified_pos | python ../bin/assign_morph_and_fix_tags_of_unks.py | python ../bin/convert_morph_field.py |python ../bin/normalize_dummy_at.py > treebanks/deptb.unified_pos.wmorph

## some manual fixes to generate treebanks/deptb.unified_pos.wmorph.fixes and treebanks/consttb.layered.unified_pos.fixes

python bin/remove_zvl_h_from_const.py < new_data/treebanks/consttb.layered.unified_pos.fixes > new_data/treebanks/consttb.layered.unified_pos.fixes2
python ../bin/retokenize_deptb.py > treebanks/deptb.unified_pos.wmorph.fixes.retoked

cp treebanks/consttb.layered.unified_pos.fixes2 treebanks/consttb.layered.final
cp treebanks/deptb.unified_pos.wmorph.fixes.retoked treebanks/deptb.final

## now some of Reut's magic produces:
#     new_data/treebanks/hebtb.sd.fixed.final-ctrees.ptb
#     new_data/treebanks/hebtb.sd.fixed.final.conll
## and then:

python bin/add_layer_to_const_trees.py new_data/treebanks/hebtb.sd.fixed.final-ctrees.ptb new_data/treeban ks/hebtb.sd.fixed.final.conll > new_data/treebanks/hebtb.sd.fixed.final-ctrees.ptb.deptags

python bin/fix_const_tb_morph.py < new_data/treebanks/hebtb.sd.fixed.final-ctrees.ptb.deptags > new_data/t reebanks/hebtb.sd.fixed.final-ctrees.ptb.deptags.morph
