import sys
sys.path.append("lattices/code")
import translate
for line in sys.stdin:
   line = line.decode("utf8").strip().split()
   if not line:
      print
      continue
   line[1] = translate.heb2tb(line[1])
   if line[1] in translate.PUNCT_TO_TB: line[1] = translate.PUNCT_TO_TB[line[1]]
   line[1] = line[1].replace("~","@suf")
   print "\t".join(line).encode("utf8")
