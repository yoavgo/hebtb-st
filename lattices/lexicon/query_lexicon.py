# coding:utf8
## Copyright 2010,2011 Yoav Goldberg
##
## This file is part of HebrewConstituencyParser
##
##    HebrewConstituencyParser is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    HebrewConstituencyParser is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with HebrewConstituencyParser.  If not, see <http://www.gnu.org/licenses/>.

import os.path
import sys; sys.path.append(os.path.join(os.path.dirname(__file__),"../lexicon/","BguLex2utf8/bin"))
import bgulex

lex = bgulex.SQBackedBGULex()
#print dir(lex)
#print list(lex.get_anals_lemmas(u"הלך"))

#lex.lex_lemma.conn.execute('select anal from lemlex where word=?',(u"הלך",))
lex.lex_lemma.conn.execute('select anal from lemlex where word=?',(unicode(sys.argv[1],"utf8"),))
for r in lex.lex_lemma.conn:
   print r

## words starting with ה
#lex.lex_lemma.conn.execute('select word from lemlex where word LIKE ?',(u"ה%",))
#hwords=[r[0] for r in lex.lex_lemma.conn]
#for w in hwords:
#   print w[1:].encode("utf8"),list(lex.get_anals_lemmas(w[1:]))
