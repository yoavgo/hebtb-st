# encoding:utf-8
## Copyright 2010,2011 Yoav Goldberg
##
## This file is part of HebrewConstituencyParser
##
##    HebrewConstituencyParser is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    HebrewConstituencyParser is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with HebrewConstituencyParser.  If not, see <http://www.gnu.org/licenses/>.

__HE = u'אבגדהוזחטיךכלםמןנסעףפץצקרשת%"'
__TB =  'ABGDHWZXJIKKLMMNNSEPPCCQRFTOU'

__FINALS   = u'םןץףך'
__REGULARS = u'מנצפכ'
__REG2FIN  =dict(zip(list(__REGULARS),list(__FINALS)))

__HE2TB = dict(zip(list(__HE),list(__TB)))
__TB2HE = dict(zip(list(__TB),list(__HE)))

__TB_2_PUNCT = {
      "yyDOT" : ".", 
      "yyCM" : ",",
      "yyQUOT" : '"',
      "yyCLN" : ";",
      "yyDASH" : "-",
      "yyLRB" : ")",
      "yyRRB" : "(",
      "yyQM" : "?",
      "yyEXCL" : "!", 
      "yySCLN" : ";", 
      "yyELPS" : "...", 
      }

__PUNCT_2_TB = {
       "." :"yyDOT", 
       "," : "yyCM",
       '"' : "yyQUOT",
       ";" : "yyCLN",
       "-" : "yyDASH",
       ")" : "yyLRB",
       "(" : "yyRRB",
       "?" : "yyQM",
       "!" : "yyEXCL", 
       ";" : "yySCLN", 
       "..." : "yyELPS", 
      }
PUNCT_TO_TB=__PUNCT_2_TB


def heb2tb(string):
   res = []
   for x in string:
      if x in __HE2TB: res.append(__HE2TB[x])
      else: res.append(x)
   return "".join(res)

def tb2heb(string):
   if string.startswith("yy"): return __TB_2_PUNCT[string]
   res = []
   for x in string:
      if x in __TB2HE: res.append(__TB2HE[x])
      else: res.append(x)
   tmp = "".join(res) 
   if res[-1] in __REGULARS and len(res) > 1:
      res[-1] = __REG2FIN[res[-1]]
      #yield "".join(res)
   #yield tmp
   return "".join(res)

if __name__=='__main__':
   import sys
   if 'e2h' in sys.argv:
      for line in sys.stdin:
         print (' '.join([tb2heb(x) for x in line.strip().split()])).encode("utf8")
   elif 'h2e' in sys.argv:
      for line in sys.stdin:
         print ' '.join([heb2tb(x) for x in line.strip().split()])
