# coding:utf8
## Copyright 2010,2011 Yoav Goldberg
##
## This file is part of HebrewConstituencyParser
##
##    HebrewConstituencyParser is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    HebrewConstituencyParser is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with HebrewConstituencyParser.  If not, see <http://www.gnu.org/licenses/>.

import sys
import re
import os.path
from codecs import open

def read_bgulex(bgulex_fh):#{{{
   lex = {}
   lex_lemma = {}
   for line in bgulex_fh:
      w,anals = line.split(None,1)
      anals = iter(anals.split())
      lex[w]=[]
      while True:
         try:
            anal = anals.next()
            lemma = anals.next()
            lex[w].append(anal)
            try:
               assert((w,anal) not in lex_lemma), " %s - %s - %s - %s" % (w,anal,lemma,lex_lemma[(w,anal)])
               lex_lemma[(w,anal)] = lemma
            except AssertionError:
               #print "2 lemmas for w+anal. skipping second. ( %s - %s - %s - %s )" % (w,anal,lemma,lex_lemma[(w,anal)])
               pass
         except StopIteration: break
   return lex, lex_lemma
#}}}

def read_bguprf(bguprf_fh):#{{{
   lex = {}
   lex_lemma = {}
   for line in bguprf_fh:
      p,anals = line.split(None,1)
      anals = iter(anals.split())
      lex[p]=[]
      while True:
         try:
            lemma = anals.next()
            anal  = anals.next()
            lex[p].append(anal)
            lex_lemma[(p,anal)] = lemma
         except StopIteration: break
   return lex, lex_lemma
#}}}

class BGULex:#{{{
   def __init__(self,lexfile=None, prffile=None,extfile=None,debug=False):#{{{
      if not lexfile or not prffile:
         folder = os.path.join( os.path.dirname(__file__) , "../data")
         #lexfile = os.path.join(folder , "bgulex.tb.hr")
         #prffile = os.path.join(folder , "bgupreflex.tb.hr")
         #extfile = os.path.join(folder , "bguextlex.tb.hr")
         lexfile = os.path.join(folder , "bgulex.utf8.hr")
         prffile = os.path.join(folder , "bgupreflex.utf8.hr")
         extfile = os.path.join(folder , "bguextlex.utf8.hr")

      self.lex, self.lex_lemma = read_bgulex(open(lexfile,encoding="utf8")) # lex items
      self.prf, self.prf_lemma = read_bguprf(open(prffile,encoding="utf8")) # prefixes

      self._lexicon_fixes()
      self._preflexicon_fixes()

      if debug:
         from collections import defaultdict
         self.lex = defaultdict(list,self.lex)


      if extfile:
         extlex, extlemma = read_bgulex(open(extfile,encoding="utf8")) # "extra" lex items
         for w,anals in extlex.iteritems():
            if w not in self.lex: self.lex[w]=[]
            for anal in anals:
               self.lex[w].append(anal)
         for (w,anal),lem in extlemma.iteritems():
            self.lex_lemma[(w,anal)]=lem

   def _lexicon_fixes(self):
      self.lex[u'אחרי'].append(':IN:')  # only AXRII is currently ":IN:"
      self.lex[u'כן'].append(':PRP-3-DEM:')

      ## getting rid of the NEG tag
      self.lex[u'בלא'].append(':IN:') 
      self.lex[u'ללא'].append(':IN:')
      self.lex[u'לא'].append(':RB:')
      if u'שלא' not in self.lex: self.lex[u'שלא'] = []
      self.lex[u'שלא'].append(':RB:')  # 'HXLIJH FLA LXDF' -> behaves like 'HXLIJH LA LXDF'
      # ECMI / ECMW / ECMH is a pronoun
      self.lex[u'עצמי'].append(':PRP-MF-S-1-REF:')
      self.lex[u'עצמנו'].append(':PRP-MF-P-1-REF:')
      self.lex[u'עצמכם'].append(':PRP-M-P-2-REF:')
      self.lex[u'עצמכן'].append(':PRP-F-P-2-REF:')
      self.lex[u'עצמך'].append(':PRP-M-S-2-REF:')
      self.lex[u'עצמך'].append(':PRP-F-S-2-REF:')
      self.lex[u'עצמה'].append(':PRP-F-S-3-REF:')
      self.lex[u'עצמו'].append(':PRP-M-S-3-REF:')
      self.lex[u'עצמם'].append(':PRP-M-P-3-REF:')
      self.lex[u'עצמן'].append(':PRP-F-S-3-REF:')

      ### add the others..
      self.lex[u'הננו'].append(':PRP-MF-P-1-PERS:')
      
      # 
      self.lex[u'מחצית'].append(':DTT:')

      # some prepositions
      self.lex[u'מעין'].append(':IN:')
      if u'מבעד' not in self.lex: self.lex[u'מבעד'] = []
      self.lex[u'מבעד'].append(':IN:')

      # percents
      if u'%' not in self.lex: self.lex[u'%'] = []
      self.lex[u'%'].append(':NN-M-SP:')

      # quotes
      if u'"' not in self.lex: self.lex[u'"'] = []
      self.lex[u'"'].append(':PUNC:')

      # these should be prefixes only
      if u'כ' in self.lex: del self.lex[u'כ']
      if u'מ' in self.lex: del self.lex[u'מ']

   def _preflexicon_fixes(self):
      # remove analyses in which W -> W H  (also for M, F)
      self.prf[u'ו'] = [x for x in self.prf[u'ו'] if x.find('DEF') == -1]
      self.prf[u'מ'] = [x for x in self.prf[u'מ'] if x.find('DEF') == -1]
      self.prf[u'ש'] = [x for x in self.prf[u'ש'] if x.find('DEF') == -1]
      # prefix H can be a relativizer
      self.prf[u'ה'].append('REL::')

#}}}

   def poses(self):
      res = set()
      for ps in self.lex.itervalues():
         for p in ps:
            res.add(p) 
      return res

   def _ortography_heuristics(self, token):#{{{
      DEF=''
      if token and token[0]=='H': # be like the rest of the lexicon: allow the definite form..
         DEF='DEF'
         token=token[1:]
      if re.match(r"[0-9]+$",token):
         yield DEF+":CD:"
      elif re.match(r"[0-9]*[.,][0-9]+[,0-9]*$",token):
         yield DEF+":CD:"
         if token.find(",") == -1: yield DEF+":NCD:"
      elif re.match(r"[0-9.:]+$",token): # : or more than one "."
         if token != ".":
            yield DEF+":NCD:"
      elif re.match(r"[^0-9]*\.[^0-9]*$",token): # dot, but no numbers
         if token != '.': yield DEF+":NNP:"#}}}
      elif re.search(r"[a-zA-Z]+",token): # foreign words.. tread as NNP
         yield ':NNP:'

   def has_definite_form(self, tanal, token):
      if "DEF" + tanal in self.get_anals(token, noPref=True):
         #print "has def form",token,tanal
         return True
      else:
         #print "NO def form",token,tanal
         return False

   def is_pref_compatible(self, tanal, prf, panal, tform=None):
      """
      implementation of MILA rules
      """
      # 9
      if "-IMPERATIVE" in tanal and prf[-1] != u'ו': return False
      # 10
      if "-PAST" in tanal and ((u'ב' in prf) or (u'כ' in prf) or (u'ל' in prf) or (u'מ' in prf)): return False
      if "-FUTURE" in tanal and ((u'ב' in prf) or (u'כ' in prf) or (u'ל' in prf) or (u'מ' in prf)): return False
      # 11 ??
      # 12 ??
      # 13
      if "-BAREINFINITIVE" in tanal and prf[-1] in u"בכלמ": return False
      # 14
      if "CC" in tanal and prf[-1] in u"בכלמ": return False
      # 15
      if prf == u'בש' and 'NNP' in tanal: return False
      # 17
      if prf == u'בש' and 'RB' in tanal: return False
      # 18
      #if "PRP" in tanal and "-PERS" in tanal and prf[-1] in "BKLM": return False

      return True

   def get_anals(self,token,noPref=False):#{{{
      assert(False),"please use get_anals_lemmas"
      ###
      ### NOTE: there is a lexicon BUG, that makes you return a def-less analysis even when there clearly is only a def one (KFHBIT)
      ###       another (the same?) BUG, returns DEF when it shouldn't (HBIT -> DEF:NNT)
      ###
      if noPref: # don't add prefixes:
         if token in self.lex:
            for anal in self.lex[token]: yield anal
         for anal in self._ortography_heuristics(token): yield anal
      else:  # do try to do prefixation
         # 0/ if word has a quote in the middle:
         spl = [x for x in token.split("U") if x]
         if len(spl) > 1:
            assert(len(spl)==2),"bad token %s" % token
            prf, tok = spl
            if prf in self.prf:
               for panal in self.prf[prf]:
                  for tanal in self.get_anals(tok):
                     yield panal + "^U^" + tanal
         # 1/ try w/o a prefix
         for anal in self.get_anals(token, noPref=True): yield anal
         # 2/ try possible prefixes 
         for prf,prfanals in self.prf.iteritems():
            if token.startswith(prf):
               for tanal in self.get_anals(token[len([x for x in prf if x!=u'ה']):], noPref=True):
                  #print "tanal is:",tanal
                  for panal in prfanals:
                     #print "panal is:",prf,panal
                     # if the prefix includes definiteness, verify that the definite form is in the lexicon for this POS
                     if 'DEF' in panal and not self.has_definite_form(tanal, token[len(prf):]):
                        pass
                        #print "skip1",panal,tanal,token[len(prf):]
                        #continue
                     if not self.is_pref_compatible(tanal, prf, panal):
                        pass
                        #print "skip2"
                        #continue
                     tanal_ = tanal
                     if not tanal[0] == ':':
                        tanal_ = ":" + ":".join(tanal.split(":")[1:])
                     yield "%s%s" % (panal.split(":")[0],tanal_)
#}}}

   def get_anals_lemmas(self,token,noPref=False):#{{{
      if token == u'לם': raise StopIteration
      if noPref: # don't add prefixes:
         if token in self.lex:
            for anal in self.lex[token]: 
               #if token[0]=='B' and 'INFINITIVE' in anal: continue # skip weired MILA analyses..
               if token[0]!=u'ל' and 'INFINITIVE' in anal: continue # skip weired MILA analyses..  (IS THIS TOO RESTRICTIVE? I don't think so.. AXWT?!)
               yield anal, self._get_lemma(token,anal)
         for anal in self._ortography_heuristics(token): yield anal,token# "_ORTO_"
      else:  # do try to do prefixation
         # 0/ if word has a quote in the middle:
         spl = [x for x in token.split(u'"') if x]
         if len(spl) > 1:
            assert(len(spl)==2),"bad token %s" % token
            prf, tok = spl
            if prf in self.prf:
               for panal in self.prf[prf]:
                  for tanal, lemma in self.get_anals_lemmas(tok):
                     yield panal + "^U^" + tanal, self._get_pref_lemma(prf,panal) + ':":'+ lemma
         # 1/ try w/o a prefix
         for anal, lemma in self.get_anals_lemmas(token, noPref=True): yield anal, lemma
         # 2/ try possible prefixes 
         for prf,prfanals in self.prf.iteritems():
            if token.startswith(prf):
               newtoks=[(False,token[len(prf):])]
               if prf[-1] in [u'כ',u'ל',u'ב']:
                  newtoks.append((True,u'ה'+token[len(prf):])) # 'H' after KLB is hidden, try to add it
               #print "newtoks are:",newtoks
               for added_h,tok in newtoks:
                  #print "TOK",tok
                  for tanal,tlemma in self.get_anals_lemmas(tok,noPref=True):
                     #print added_h,tok,tanal, tlemma
                     # if we added an 'H', make sure what we got back was indeed something (N,J,BN...) with a definiteness
                     if added_h and tok[0]==u'ה' and 'DEF' not in tanal: 
                        #print "skipping",tok,tanal
                        continue  # TODO: still can be to H's, I'm not covering that
                     # if we didn't add an 'H', make sure we do not allow a definite reading after K L B
                     if not added_h and tok[0]==u'ה' and 'DEF' in tanal and prf[-1] in [u'כ',u'ל',u'ב']:
                        continue
                     #if added_h and tok=='HW': continue  # TODO: still can be to H's, I'm not covering that
                     for panal in prfanals:
                        if not self.is_pref_compatible(tanal, prf, panal, tok):
                           #print >> sys.stderr, "incompatible:",prf,tok,tanal,panal
                           continue
                     #print "@@",tok,tanal,panal
                     tanal_ = tanal
                     if not tanal[0] == ':':
                        assert(tanal.startswith("DEF:"))
                        #print "tanal DEF?"
                        tanal_ = ":" + ":".join(tanal.split(":")[1:]) # remove the first DEF
                                                                      # and move it to the prefix
                        res= "%s%s" % (panal.split(":")[0]+"+DEF",tanal_), self._get_pref_lemma(prf,panal)+u"^ה" + ":" + tlemma
                     else:
                        res= "%s%s" % (panal.split(":")[0],tanal_), self._get_pref_lemma(prf,panal) + ":" + tlemma
                     #print "RES:",res
                     yield res
#}}}

   def _get_lemma(self, token, anal):
      if anal.find("^U^") > -1:
         anal = anal.split("^U^")[1]
         token = token.split(u'"',1)[1]
      if (token, anal) in self.lex_lemma:
         return self.lex_lemma[(token,anal)]
      else:
         assert(anal.find("S_PRN")==-1 or token not in self.lex), "we should have lemmas for all pronomial suffixed items.."
         #print "no lemma for",token
         return token

   def _get_pref_lemma(self, prf, anal):
      if prf == u'ה' and anal == 'REL::': return u'ה'
      return self.prf_lemma[(prf,anal)]

   def get_lemma(self, token, anal):
      """
      lemma: pref1^pref2^pref3:lemma
      """
      res = [lemma for _anal,lemma in self.get_anals_lemmas(token) if anal == _anal]
      assert(len(res)==1)
      return res[0]


#}}}

class PackedBGULexicon:#{{{ deprecated
   """
   a lexicon packed to a specific wordlist
   """
   def __init__(self):
      self.lex = None

   @classmethod
   def from_file(cls, fname="bgulex.packed.tb.hr"):
      inst = cls()
      inst.lex = {}
      for line in file(fname):
         line = line.strip().split()
         word = line[0]
         anals = line[1:]
         inst.lex[word]=anals
      return inst

   @classmethod
   def from_lexicon(cls, lex, wordlist):
      plex = {}
      wordlist = set(wordlist)
      for word in sorted(wordlist):
         plex[word] = lex.get_anals(word)
      
      inst = cls()
      inst.lex = plex
      return inst

   def to_file(self, fname="bgulex.packed.tb.hr"):
      fh = file(fname,"w")
      for w,a in self.lex.iteritems():
         fh.write("%s %s\n" % (w," ".join(a)))
      fh.close()

   def get_anals(self,word):
      if word in ",.!?-:;()" : return [':PUNC:']
      if word == '...': return [':PUNC:']
      return self.lex[word]
   #}}}

class SQBackedBGULex(BGULex):
   def __init__(self,lexfile=None, prffile=None,extfile=None,debug=False):#{{{
      import sqbackend
      if not prffile:
         folder = os.path.join( os.path.dirname(__file__) , "../data")
         lexfile = os.path.join(folder , "lexicon.utf8.sqlite")
         prffile = os.path.join(folder , "bgupreflex.utf8.hr")
         #extfile = os.path.join(folder , "bguextlex.tb.hr")

      #print "creating"
      self.lex, self.lex_lemma = sqbackend.create_lex(lexfile)
      self.prf, self.prf_lemma = read_bguprf(open(prffile,encoding="utf8")) # prefixes

      # NOTE: we are not doing the _lexicon_fixes() here.  if added to that class, must generate the sqlite db again (see sqbackend)
      self._preflexicon_fixes()

      if extfile:
         assert(False),"not supported yet"
         extlex, extlemma = read_bgulex(open(extfile,encoding="utf8")) # "extra" lex items
         for w,anals in extlex.iteritems():
            if w not in self.lex: self.lex[w]=[]
            for anal in anals:
               self.lex[w].append(anal)
         for (w,anal),lem in extlemma.iteritems():
            self.lex_lemma[(w,anal)]=lem

if __name__ == '__main__':
   # pack lexicon to wordlist from postags
   import sys

   lex = SQBackedBGULex()
   print sys.argv[1]
   for anal,form in list(lex.get_anals_lemmas(unicode(sys.argv[1],"utf8"))):
      print anal,form.encode("utf8")

   sys.exit()
   #print "LHWRID",list(lex.get_anals_lemmas("LHWRID"))
   # HHCBEH! should work
   #print "WHRWWXH",list(lex.get_anals_lemmas("WHRWWXH"))
   #print
   print "LBDW",list(lex.get_anals_lemmas("LBDW"))
   print
   print "WMHBIT",list(lex.get_anals_lemmas("WMHBIT"))
   print "HBIT",list(lex.get_anals_lemmas("HBIT"))
   print "KFHBIT",list(lex.get_anals_lemmas("KFHBIT"))
   print "MHBIT",list(lex.get_anals_lemmas("MHBIT"))
   print "BBIT",list(lex.get_anals_lemmas("BBIT"))

   def yield_tokens_and_anals_from_postags(postags_fh):
      for line in postags_fh:
         if line.startswith("#"): continue
         line = line.strip()
         if not line: continue
         token,anal = line.split(None,1)
         yield token, anal

   LEXFILE     = sys.argv[1]
   PREFLEXFILE = sys.argv[2]
   VOCABFILE   = sys.argv[3]
   PACKEDLEX   = sys.argv[4]

   lex = BGULex(LEXFILE, PREFLEXFILE)
   wl = [token for token,anal in yield_tokens_and_anals_from_postags(file(VOCABFILE))]
   plex = PackedBGULexicon.from_lexicon(lex, wl)
   plex.to_file(PACKEDLEX)


      
