# coding:utf8
## Copyright 2010,2011 Yoav Goldberg
##
## This file is part of HebrewConstituencyParser
##
##    HebrewConstituencyParser is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    HebrewConstituencyParser is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with HebrewConstituencyParser.  If not, see <http://www.gnu.org/licenses/>.

import sqlite3

if __name__=='__main__':
   # prepare the lexicon file

   conn = sqlite3.connect("../data/lexicon.utf8.sqlite")
   c = conn.cursor()
   c.execute(u"delete from lemlex where word='תחננו'")
   c.execute(u"select * from lemlex where word='תחננו'")
   c.execute(u"insert into lemlex values ('תחננו',':VB-FUTURE-PAAL-MF-P-2:','חנן')")
   c.execute(u"select * from lemlex where word='תחננו'")
   for row in c:
      print row
   conn.commit()


