## Copyright 2010,2011 Yoav Goldberg
##
## This file is part of HebrewConstituencyParser
##
##    HebrewConstituencyParser is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    HebrewConstituencyParser is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with HebrewConstituencyParser.  If not, see <http://www.gnu.org/licenses/>.

import codecs
import sys
import bgutags

try:
   infile = sys.argv[1]
except IndexError:
   infile = "../data/bgulex.utf8"

for line in codecs.open(infile,"r","utf-8"):#{{{
   w,anals = line.strip().split(None,1)
   print w.encode("utf-8"),
   anals = iter(anals.split())
   while True:
      try:
         print bgutags.bm2tag(int(anals.next())),
         print anals.next().encode("utf-8"),
      except StopIteration: break
   print#}}}

