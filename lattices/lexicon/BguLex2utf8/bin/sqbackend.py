# coding:utf8
## Copyright 2010,2011 Yoav Goldberg
##
## This file is part of HebrewConstituencyParser
##
##    HebrewConstituencyParser is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    HebrewConstituencyParser is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with HebrewConstituencyParser.  If not, see <http://www.gnu.org/licenses/>.

import time
import sqlite3
# an qslite backend for the lexicon -- may be slower, but 0 loading time

class SQLexLemma:
   def __init__(self, conn):
      self.conn=conn
      self.cache={}

   def __getitem__(self, item):
      try:
         return self.cache[item]
      except KeyError:
         cursor = self.conn#.cursor()
         w,anal = item
         stime=time.time()
         cursor.execute('select lemma from lemlex where word=? and anal=?',(w,anal))
         #print "w,anal",time.time()-stime
         #res = cursor.next()[0]
         res = cursor.fetchone() #.next()[0]
         #cursor.close()
      if res: 
         self.cache[item]=res[0]
         return res[0]
      return

   def __contains__(self, item):
      return self[item] is not []

class SQLexicon:
   def __init__(self, conn):
      self.conn=conn
      self.cache={}

   def __contains__(self, item):
      return self[item] is not []

   def __getitem__(self, item):
      #print "gettig anals for item",item
      try:
         return self.cache[item]
      except KeyError:
         cursor = self.conn#.cursor()
         w = item
         #stime=time.time()
         cursor.execute('select anal from lemlex where word=?',(w,))
         #print "anal",time.time()-stime
         res=[row[0] for row in cursor]
         #cursor.close()
         if res: self.cache[item]=res
         return res

def create_lex(db_fname):
   conn = sqlite3.connect(db_fname)
   conn.execute('PRAGMA temp_store=MEMORY;')
   conn.execute('PRAGMA journal_mode=MEMORY;')
   conn.execute('PRAGMA cache_size=400;')
   conn = conn.cursor()
   lex_lem = SQLexLemma(conn)
   lex     = SQLexicon(conn)
   return lex,lex_lem


def store_lexicon_to_db(lexicon,db_fname):
   conn = sqlite3.connect(db_fname)
   c = conn.cursor()

   # Create tables
   print "creating table lemlex"
   c.execute('''create table lemlex (word text, anal text, lemma text)''')

   print "inserting data"
   # Insert a row of data
   for (w,anal),lem in lexicon.lex_lemma.iteritems():
      c.execute("""insert into lemlex values (?,?,?)""", (w,anal,lem))

   c.execute('''create index lemlex_idx on lemlex (word)''')
   #c.execute('''create index lemlex_idx2 on lemlex (word,anal)''')

   # Save (commit) the changes
   conn.commit()

   # We can also close the cursor if we are done with it
   c.close()

if __name__=='__main__':
   # prepare the lexicon file

   from bgulex import *
   #lexicon = BGULex()
   #store_lexicon_to_db(lexicon,"../data/lexicon.sqlite")
   lexicon = BGULex()
   store_lexicon_to_db(lexicon,"../data/lexicon.utf8.sqlite")

   import sys
   sys.exit()
   conn = sqlite3.connect("../data/lexicon.utf8.sqlite")
   c = conn.cursor()
   #c.execute("select anal from lemlex where word='BITW'")
   c.execute(u"select anal from lemlex where word='ביתו'")
   for row in c:
      print row


