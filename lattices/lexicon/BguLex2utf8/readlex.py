## Copyright 2010,2011 Yoav Goldberg
##
## This file is part of HebrewConstituencyParser
##
##    HebrewConstituencyParser is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    HebrewConstituencyParser is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with HebrewConstituencyParser.  If not, see <http://www.gnu.org/licenses/>.

from collections import defaultdict

def read(lexfile):
   d = defaultdict(list)
   for line in lexfile:
      line = line.strip().split()
      word=line[0]
      rest = iter(line[1:])
      while True:
         try:
            anal  = rest.next()
            lemma = rest.next()
            d[word].append((anal,lemma))
         except StopIteration: break
   return d

if __name__=='__main__':
   import random
   import codecs
   lex = read(codecs.open("bgulex.utf8.hr",encoding="utf-8"))
   aWord = random.choice(lex.keys() )
   print aWord.encode("utf-8")
   for anal, lemma in lex[aWord]:
      print "\t",anal.encode("utf-8"),lemma.encode("utf-8")



