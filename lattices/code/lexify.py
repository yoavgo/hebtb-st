#!/usr/bin/env python
## Copyright 2010,2011 Yoav Goldberg
##
## This file is part of HebrewConstituencyParser
##
##    HebrewConstituencyParser is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    HebrewConstituencyParser is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with HebrewConstituencyParser.  If not, see <http://www.gnu.org/licenses/>.

import sys
import os
import os.path
from codecs import open
#import sys; sys.path.append(os.path.join(os.path.dirname(__file__),"../lexicon/BguLex2/bin/")) 
#import bgulex

print >> sys.stderr, "loading data-driven lexicon"
WORDCOUNTS_FILE=os.path.join(os.path.dirname(__file__),"../lexicon/full_nodef.twcount")
from ddl import DataDrivelLexicon
lex=DataDrivelLexicon(WORDCOUNTS_FILE,short=False)
print >> sys.stderr,"done loading"

def is_singleton_tb_tag(tag_name):
   if tag_name[0]!=':': return False
   if tag_name[-1]==':': return True
   if tag_name[-1]!=':' and tag_name[1]=='N': return True
   return False

REMOVE=set("HUFAL PUAL HIFIL HITPAEL FUTURE 1 3 2 IMPERATIVE PAST NIFAL POSITIVE PAAL NEGATIVE PAST PIEL FUTURE".split())
REMOVE.add('')

def clear_tags(tag):
   tag = '-'.join([p for p in tag.replace(":","-").split('-') if p not in REMOVE])
   return tag

def get_tags(word,unif=False):
   if word.startswith('yy'): return ["%s/1.0" % word] # punctuations
   t_ps = lex.get_tags_tgws(word)
   if unif and t_ps:
      prob = 1.0/len(t_ps)
      t_ps = ["%s/%s" % (x.split("/")[0],prob) for x in t_ps]
   if not t_ps:
      return ['UNK/1.0']
   else:
      return t_ps

#for w in "LRWXB KAMWR BHEDR BTBWNH KWLNW".split():
#   print w,list(lex.get_anals_lemmas(w,noPref=True))
#sys.exit(1)

parts=set()
def annotate_input(fh,UNIF,out=sys.stdout):
   for line in fh:
      for word in line.split():
         tags = get_tags(word,unif=UNIF)
         print >> out, "%s@!@%s" % (word, '@'.join(tags)),
         for tag in get_tags(word):
            for x in tag.split('-')[1:]: parts.add(x)
      print >> out

def annotate_trees(fh,UNIF):
   unk_words=set()
   sys.path.append(os.path.join(os.path.dirname(__file__),"pytrees/"))
   from tree_readers import read_trees_oneperline_file
   for t in read_trees_oneperline_file(fh):
      for l in t.collect_leaves():
         word = l.get_word()
         tags = get_tags(word,unif=UNIF)
         l.set_word("%s@!@%s" % (word, '@'.join(tags)))
      print ("( %s )" % t.as_sexpr()).encode("utf8")
   out = open("unkwords","w","utf8")
   for w in unk_words: print >> out,w
   out.close()

def annotate_trees_for_em_input(fh,UNIF):
   print "ho"
   unk_words=set()
   sys.path.append(os.path.join(os.path.dirname(__file__),"pytrees/"))
   from tree_readers import read_trees_oneperline_file
   print "xf"
   for t in read_trees_oneperline_file(fh):
      for l in t.collect_leaves():
         word = l.get_word()
         tags = get_tags(word,unif=UNIF)
         print l.get_word(),l.get_pos(),' '.join(tags)
         if 'UNK' in tags: unk_words.add(word)
   out = file("unkwords","w")
   for w in unk_words: print >> out,w
   out.close()

def _read_lats(fh):
   lat=[]
   seen=set()
   for line in fh:
      line = line.strip().split()
      if not line: 
         if lat: yield lat
         lat=[]
         seen=set()
         continue
      s=line[0]
      e=line[1]
      w=line[2]
      if (s,e,w) in seen: continue
      lat.append((s,e,w))
      seen.add((s,e,w))
   if lat: yield lat

def annotate_lattices(fh,UNIF, out=sys.stdout):
   for lat in _read_lats(fh):
      for s,e,w in lat:
         tags = get_tags(w,unif=UNIF)
         print >> out, s,e,("%s@!@%s" % (w, '@'.join(tags))).encode("utf8")
      print >> out
      out.flush()



if __name__=='__main__':
   import sys
   UNIF='unif' in sys.argv
   import codecs
   (utf8_encode, utf8_decode, utf8_reader, utf8_writer) = codecs.lookup('utf-8')
   instream = utf8_reader(sys.stdin)

   if 'input' in sys.argv:
      annotate_input(instream,UNIF)
   elif 'trees' in sys.argv:
      annotate_trees(instream,UNIF)
   elif 'trees_words' in sys.argv:
      annotate_trees_for_em_input(instream,UNIF)

   elif 'lat' in sys.argv:
      annotate_lattices(instream,UNIF)


#for x in parts:
#   print "@",x



