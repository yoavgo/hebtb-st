# encoding: utf8
#TODO deal with: in lat: NEG, NNPT, PUNC, TTL in tb: NCD, DUMMY_AT, yyXXX, ZVL

#TODO: O in DataDrivenAnalyzer, segmentation in DataDrivenAnalyzer

#!/usr/bin/env python
## Copyright 2010,2011 Yoav Goldberg
##
## This file is part of HebrewConstituencyParser
##
##    HebrewConstituencyParser is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    HebrewConstituencyParser is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with HebrewConstituencyParser.  If not, see <http://www.gnu.org/licenses/>.
import sys
import tbanalyzer   # does segmentation according to TB (pure)
import translate
import os.path

HERE = os.path.dirname(__file__)
sys.path.append(HERE + "/../../bin")
from convert_morph_field import convert_morph

class DataDrivenAnalyzer:
   def __init__(self,fname=HERE + "/../data_driven_anal_table"):
      self.d = {}
      for line in file(fname):
         line = line.strip().split()
         if line[0][0] == '#': continue
         w = line[0]
         anals = line[1:]
         if not anals: continue
         self.d[w] = anals
   def get_anals(self, w):
      if w in translate.PUNCT_TO_TB: 
         return [translate.PUNCT_TO_TB[w]]
      d = self.d
      if w in d: return d[w]
      sig = "UNK_%s" % w[-2:]
      if sig in d: return d[sig]
      sig = "UNK_%s" % w[-1:]
      if sig in d: return d[sig]
      return d['UNK']


class TokenLat:
   """
   self.lattice: a set of elemnts, each of the of the form ((morph,pos),(start,end))
   where (start,end) is a pair of integers 
   """
   def __init__(self, tok_idx, anals, start=0):
      """
      anals: list of anal elements.
               each anal is of the form ((POS,MORPH,(start,end)),(POS,MORPH,(start,end)))
             where "start","end" are in abstract "string positions" of the original token
             the first 'start's of all anals should be the same
             the last  'end' of all anals should be the same
      """
      self.start = start
      self.end = None
      self._anals   = list(anals)
      self.lattice = self._anals_to_lat(self._anals)
      self.tok_idx = tok_idx

   def arcs(self):
      return self.lattice

   def anals(self): 
      return self._anals

   def _anals_to_lat(self, anals): #{{{
      #print "num of anals:",len(anals)
      #print anals

      anals = list(anals)
      # verify precondition on anals structure (all of them start/end at the same id)
      starts = [anal[0][2][0] for anal in anals]
      ends   = [anal[-1][2][1] for anal in anals]
      #print anals
      #print starts
      #print ends
      try:
         assert len(set(ends))==1,(anals,ends)
      except AssertionError,e: print >> sys.stderr, "failed assertion 2:",e
      try:
         assert len(set(starts))==1,(anals,starts)
      except AssertionError,e: print >> sys.stderr, "failed assertion 3:",e
      # done verifying precondition

      # remap state numbers so that all starts and end are integers, keep same order
      states = set()
      for anal in anals:
         for pos,morph,(start,end) in anal:
            states.add(start)
            states.add(end)
      statemap={}
      for newstate,oldstate in enumerate(sorted(states),self.start):
         statemap[oldstate]=newstate

      self.end = max(statemap.values()) 

      # set the anals with the new state-ids
      returned_arcs=set()
      for anal in anals:
         for pos,morph,(start,end) in anal:
            returned_arcs.add((pos,morph,(statemap[start],statemap[end])))

      return returned_arcs
      #}}}

   def _OLD_anals_to_lat(self, anals): #{{{
      #print "num of anals:",len(anals)
      #print anals
      anals = list(sorted(anals, key=len, reverse=True))
      states = range(self.start, self.start+len(anals[0])+1) # as many states as the longest anal+1
      last_state = states[-1]
      self.end = last_state

      returned_arcs = set() # elements of these set will be (label,(from,to))
      assigned_arcs = dict()
      for anal in anals:
         #print "handling",anal
         current_anal_assigned_arcs = dict()
         # stage 1: assign constrained arcs (these which are already assigned)
         if len(anal)>1:
            for posmorph in anal:
               if posmorph in assigned_arcs:
                  current_anal_assigned_arcs[posmorph] = assigned_arcs[posmorph]
         # stage 2: fill in all the un-assigned arcs
         for _i,(i,posmorph) in enumerate(zip(states,anal)):
            if posmorph in current_anal_assigned_arcs and len(anal)==1: continue
            else:
               try:
                  next_morph = anal[_i+1]
                  if next_morph in current_anal_assigned_arcs:
                     # if next morph is assigned, I want to get all the way to it
                     location = (i,current_anal_assigned_arcs[next_morph][0])
                  else:
                     # otherwise, I want to take up just one space
                     location = (i, i+1)
               except IndexError: # we were at the last morph, there's no next..
                  location = (i,last_state)
               current_anal_assigned_arcs[posmorph] = location
               assigned_arcs[posmorph] = location
         # stage 3: if there are gaps, fill them by attaching to the next morph
         last_morph_end = self.start
         for i,posmorph in zip(states,anal):
            (start,end) = current_anal_assigned_arcs[posmorph]
            if start != last_morph_end: 
               #print "Joining back!",posmorph[i-1]
               start = last_morph_end
            if start != end: returned_arcs.add((posmorph, (start,end)))
            last_morph_end = end
      return returned_arcs
      #}}}

   def as_dot(self, fh): #TODO: this one was not updated to new representation
      states = set()
      arcs = set()
      for label, (start, end) in self.arcs():
         states.add(start)
         states.add(end)
         morph,pos = label
         arcs.add((start,end,"%s(%s)" % (morph, pos)))   
      ret = ["digraph lat { rankdir=LR; node[shape=circle];"]
      for (start,end,label) in arcs:
         ret.append('%s -> %s [ label = "%s"];' % (start, end, label))
      ret.append("}")

      fh.write( "\n".join(ret) )
         
class SentenceLat:
   def __init__(self, analyzer, tokens):
      self._lattices = []
      start = 0
      for idx,tok in enumerate(tokens,1):
         segs = analyzer.get_segmentations(tok)
         segs = [x for x in segs if x]
         #print "segs for",tok.encode("utf8"),":",segs
         # TODO: add data-driven segmentation also.
         if not segs:
            segs = [((tok,pos,(0,len(tok))),) for pos in analyzer.pos_guesser.get_anals(translate.heb2tb(tok))]
            assert(segs),translate.heb2tb(tok)
         lat = TokenLat(idx,segs,start)
         start = lat.end
         self._lattices.append(lat)
         #for m,p,(s,e) in lat.arcs():
         #   if not e > s:
         #      print >> sys.stderr, "AA",tok,s,e
   def arcs(self):
      for lat in self._lattices:
         tok_idx = lat.tok_idx
         for arc in set(lat.arcs()):
            #print "arc is:",arc
            yield tok_idx,arc
            
   def as_seg_lat(self, out, pos_transformer=lambda x:x,lprobs=None):
      arcs = sorted(self.arcs(),key=lambda (tok_idx,(morph,pos,(s,e))): (s,e))
      for tok_idx,arc in arcs:
         morph,pos,(s,e) = arc
         if morph not in  ['H','AT',u"את",u"ה"]:
            if not e>s:
               print >> sys.stderr, morph
               print >> sys.stderr,"@@end must be > start, skipping anal (%s,%s,%s)" % (morph.encode("utf8"),s,e)
               #sys.exit()
         if morph is None: continue
         if morph == '###NUMBER###': morph = '500'
         if morph in translate.PUNCT_TO_TB: 
            morph = translate.PUNCT_TO_TB[morph]
            #if morph == '(': morph='yyLRB'
            #if morph == ')': morph='yyRRB'
            #morph = morph.replace("(","yyLRB").replace(")","yyRRB")
            pos   = morph
         morph = translate.heb2tb(morph)
         #if not pos:
         #   poses = analyzer.pos_guesser
         if pos:
            #pos = pos_transformer(pos)
            cpos,fpos,mfeats = sep_tag_and_morph(pos)
            pos = cpos
            #if "_TOINF" in pos: pos="VBINF"
            if (pos not in allowed_tags) and (len(allowed_tags)>0):
               # try to do a heuristic match to close-by tags in the allowed
               #pos = pos.split("_")[0]
               alternatives = [t for t in allowed_tags if t.split("_")[0]==pos]
               if alternatives:
                  for pos in alternatives: 
                     if lprobs!=None: 
                        prob = lprobs.get_TgW(pos,translate.tb2heb(morph))
                        out.write(("%s %s %s %s %s\n" % (s,e,morph,pos,prob)).encode("utf8"))
                     else:
                        out.write(("%s %s %s %s\n" % (s,e,morph,pos)).encode("utf8"))
               else:
                  # if still not successful, leave empty (this means "everything")
                  if pos not in ['NEG','TTL']:
                     print >> sys.stderr,"unknown tag:",pos
                  out.write(("%s %s %s\n" % (s,e,morph)).encode("utf8"))
            else:
               if lprobs!=None: 
                  prob = lprobs.get_TgW(pos,translate.tb2heb(morph))
                  out.write(("%s %s %s %s %s\n" % (s,e,morph,pos,prob)).encode("utf8"))
               else:
                  out.write(("%s\t%s\t%s\t_\t%s\t%s\t%s\t%s\n" % (s,e,morph,cpos,fpos,mfeats,tok_idx)).encode("utf8"))
         else:
            out.write(("NO POS %s %s %s\n" % (s,e,morph)).encode("utf8"))
      out.write("\n")
      out.flush()


GENDER=['M','F']
NUMBER=['P','S','D']
TENSE=['FUTURE','PAST']
OTHER=['TOINFINITIVE'] #,'SUB',
TO_KEEP = set(GENDER+NUMBER+TENSE+OTHER)

def feature_remover_1(pos_with_features):
   global TO_KEEP
   pos_with_features = pos_with_features.split("/")[0].split("-")
   pos = pos_with_features[0]
   features = [x for x in pos_with_features[1:] if x in TO_KEEP]
   return '_'.join([pos]+features)

def feature_remover_all(pos_with_features):
   pos_with_features = pos_with_features.replace("S_PRN","PRP").replace("S_ANP","PRP").replace("VB-TOINF","VBINF-")
   pos_with_features = pos_with_features.replace("S_PP","PRP") # are we sure?
   assert('_' not in pos_with_features.split("-")[0])
   return pos_with_features.split("-")[0]

def sep_tag_and_morph(pos):
   if ":" in pos:
      pos,mfeats = pos.split(":")
      return pos,pos,mfeats
   orig = pos
   if "/" in pos:
      pos,sufpos = pos.split("/")
      pos,morph = pos.split("-",1)
      sufpos,sufmorph = sufpos.split("-",1)
      assert(pos in ['NN','BN','CD']),pos
      pos = pos+"/" +sufpos
      morph = morph.split("-")
      sufmorph = [("suf_%s" % m) for m in sufmorph.split("-")]
      morph = morph + sufmorph
   else:
      morph = []
      if '-' in pos:
         pos, morph = pos.split("-",1)
         morph = morph.split("-")
      if morph and 'SUBCONJ' in morph:
         pos = pos + "-" + 'SUBCONJ'
         morph.remove('SUBCONJ')
      if morph and 'TOINFINITIVE' in morph:
         pos = pos + "-" + 'TOINFINITIVE'
         morph.remove('TOINFINITIVE')
   assert('TOINFINITIVE' not in morph),orig
   morph,fpos = convert_morph(morph,pos)
   if morph == ['']: morph = None
   return pos,fpos,("|".join(morph) if morph else '_')

def make_debug_data(analyzer):
   import pickle
   segmentations=dict()
   for TOK in 'KFHM LPIH AXRI BW MFMFIM FMIIM KXWLIM'.split():
      segmentations[TOK]=analyzer.get_segmentations(TOK)
   pickle.dump(segmentations,file("somea_anals.pkl","w"))
   sys.exit()

def load_debug_data():
   import pickle
   return pickle.load(file("somea_anals.pkl"))

class AllowAll:
   def __contains__(self,item): return True

allowed_tags=[]
if __name__=='__main__':
   #sys.exit()
   TAGPROBS=False
   NOMORPH=False

   if TAGPROBS:
      import os.path
      sys.path.append(os.path.join(os.path.dirname(__file__),"../wordprobs"))
      import lexprobs
      if NOMORPH:
         lprobs = lexprobs.LexProb("simple_tags")
      else:
         lprobs = lexprobs.LexProb("morph1_tags")

   #allowed_tags = set([l.strip() for l in file(os.path.join(os.path.dirname(__file__),"training_tags"))])
   if '-allowall' in sys.argv: allowed_tags = AllowAll()
   print >> sys.stderr, "allowed tags:",allowed_tags
   analyzer = tbanalyzer.AnalyzerWpos(always_include_full=False)
   pos_guesser = DataDrivenAnalyzer()
   analyzer.pos_guesser = pos_guesser
   #for sent in sys.stdin:
   sent = sys.stdin.readline()
   while sent.strip():
      sent = sent.decode("utf8")
      #print >> sys.stdout, "#",sent
      toks = sent.split()
      toks = [translate.tb2heb(t) for t in toks]
      slat = SentenceLat(analyzer,toks)
      if TAGPROBS:
         if NOMORPH:
            slat.as_seg_lat(sys.stdout, feature_remover_all, lprobs)
         else:
            slat.as_seg_lat(sys.stdout, feature_remover_1, lprobs)
      else:
         if NOMORPH:
            slat.as_seg_lat(sys.stdout, feature_remover_all)
         else:
            slat.as_seg_lat(sys.stdout, feature_remover_1)
      sent = sys.stdin.readline()

