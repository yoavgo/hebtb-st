# coding:utf8
## Copyright 2010,2011 Yoav Goldberg
##
## This file is part of HebrewConstituencyParser
##
##    HebrewConstituencyParser is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    HebrewConstituencyParser is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with HebrewConstituencyParser.  If not, see <http://www.gnu.org/licenses/>.



# produce segmentations compatible with the Hebrew Treebank.
# (that is, at least one of the segmentations given for a token should compatible with 
#  the analysis for that token in the .postags file)

from collections import defaultdict
import kcpostags as kcp
import os.path
from codecs import open

#PREFLEX_FILE = os.path.join(os.path.dirname(__file__),"../lexicons/BguLex/data/bgupreflex.tb.hr")
PREFLEX_FILE = os.path.join(os.path.dirname(__file__),"../lexicons/BguLex2/data/bgupreflex.utf8.hr")

def __read_bgupreflex(filename=None):
   if not filename:
      import os.path
      filename = os.path.join(PREFLEX_FILE)
   prefixes = defaultdict(set)
   for line in open(filename,encoding="utf8"):
      line = line.strip().split()
      pref = line[0]
      dat = iter(line[1:])
      while True:
         try:
            forms = tuple(dat.next().split("^"))
            poses = tuple(dat.next().split("+"))
            prefixes[pref].add((forms,poses))
         except StopIteration:
            break
   return prefixes
         

#prefixes = read_bgupreflex()

SPECIAL_FORMS = {
      u'לה' : [(u'ל',u'היא'),(u'לה',)],
      u'לי' : [(u'ל',u'אני'),(u'לי',)],
      u'לו' : [(u'ל',u'הוא')],
      u'לנו' : [(u'ל',u'אנחנו'),(u'לנו',)],
      u'לך' : [(u'ל',u'אתה'),(u'ל',u'את'),(u'לך',)],

      u'עלי'   : [(u'עלי',u'אני'),(u'עלי',)],
      u'עלינם' : [(u'עלי',u'הם')],
      u'עלינן' : [(u'עלי',u'הן')],
      u'עלינו' : [(u'עלי',u'אנחנו')],
      u'עליכם' : [(u'עלי',u'אתם')],
      u'אליכן' : [(u'עלי',u'אתן')],
      u'עליו'  : [(u'עלי',u'הוא')],
      u'עליה'  : [(u'עלי',u'היא')],

      u'אלי'   : [(u'אלי',u'אני'),(u'אלי',)],
      u'אלינם' : [(u'אלי',u'הם')],
      u'אלינן' : [(u'אלי',u'הן')],
      u'אלינו' : [(u'אלי',u'אנחנו')],
      u'אליכם' : [(u'אלי',u'אתם')],
      u'אליכן' : [(u'אלי',u'אתן')],
      u'אליו'  : [(u'אלי',u'הוא')],
      u'אליה'  : [(u'אלי',u'היא')],

      u'ממך'   : [(u'מ',u'אתה'),(u'מ',u'את')],
      u'ממני'   : [(u'מ',u'אני')],

      }

class Analyzer_o:
   def __init__(self):
      self._prefixes=prefixes
      # _prefixes_list is sorted by length (reverse)
      self._prefixes_list = list()
      for pref,anal in prefixes.items():
         self._prefixes_list.extend([(pref,(forms,poses)) for forms,poses in anal])
      self._prefixes_list = sorted(self._prefixes_list,key=lambda x:-len(x[0]))

   def _split_prefs(self,word):
      for pref, pref_anals in self._prefixes.items():
         if word.startswith(pref):
            for p_forms,p_poses in pref_anals:
               yield p_forms,word[len(pref):]
      yield ([],word)

   def _split_main(self,main):
      if main in SPECIAL_FORMS:
         for parts in SPECIAL_FORMS[main]:
            yield parts
      else:
         yield (main,)

   def get_segmentations(self, word):
      prefs_rests = self._split_prefs(word)
      for pref,rest in prefs_rests:
         forms = list(pref)
         for main_parts in self._split_main(rest):
            yield tuple(forms+list(main_parts))

def _SUFF_TO_PRN(tok,m,p):
   if m==u'ה': return u'היא'
   elif m==u'ו': return u'הוא'
   elif m==u'ם': return u'הם'
   elif m==u'נ': return u'הן'
   elif m==u'י': return u'אני'
   elif m==u'נו': return u'אנחנו'
   elif m==u'הם': return u'הם'
   elif m==u'הן': return u'הן'
   elif m==u'כם': return u'אתם'
   elif m==u'כן': return u'אתן'
   elif m==u'כ':
      if 'F' in p: return u'את'
      else: return u'אתה'
   return m+"_unk"

def _get_raw_tb_morphs_from_anal(tok,anal):
   """
   anal is [(MORPH,POS),(MORPH,POS),...]
   """
   res=[]
   for m,p in anal:
      m=m.replace("/","") # those suffixes are not marked in TB forms
      if p == 'DEF' and m=='DUMMY_H':
         res.append(u'ה')
      elif p.startswith('DEF:') and m.startswith(u'ה'):
         res.append(m[1:])
      elif p.startswith("S_PRN") or p.startswith("S_ANP"):
         res.append(_SUFF_TO_PRN(tok,m,p))

      elif m=='_AT_' and anal[-1][1].startswith('S_ANP'):
         res.append(u'אות')

      elif m==u'אל' and anal[-1][1].startswith('S_PRN') and u'אלי' in tok:
         if tok==u'אלי': res.append(u'אל')
         else: res.append(u'אלי')
      elif m==u'על' and anal[-1][1].startswith('S_PRN') and u'עלי' in tok:
         if tok==u'עלי': res.append(u'על')
         else: res.append(u'עלי')
      elif m==u'את' and anal[-1][1].startswith('S_PRN') and u'אות' in tok:
         res.append(u'אות')
      elif m==u'בין' and anal[-1][1].startswith('S_PRN') and u'ביני' in tok:
         res.append(u'ביני')
      else:
         res.append(m)
   return tuple(res)

def _get_raw_tb_morphs_and_kc_pos_from_anal(tok,anal):
   """
   anal is [(MORPH,POS,(start,end)),(MORPH,POS,(start,end)),...]
             where "start","end" are in abstract "string positions" of the original token
             the first 'start's of all anals should be the same
             the last  'end' of all anals should be the same
   """
   res=[]
   for m,p,(start,end) in anal:
      m=m.replace("/","") # those suffixes are not marked in TB forms
      if p == 'DEF' and m=='DUMMY_H':
         res.append((u'ה',p,(start,end)))
      elif p.startswith('DEF:') and m.startswith(u'ה'):
         res.append((m[1:],p.split(":")[1],(start,end)))
      elif p.startswith("S_PRN") or p.startswith("S_ANP"):
         res.append((_SUFF_TO_PRN(tok,m,p),p,(start,end))) #TODO: add proper pronoun pos

      elif m=='_AT_' and anal[-1][1].startswith('S_ANP'):
         res.append((u'אות','DUMMY_AT',(start,end)))  #YG: changed AT to DUMMY_AT

      elif m==u'אל' and anal[-1][1].startswith('S_PRN') and u'אלי' in tok:
         if tok==u'אלי': res.append((u'אל','IN',(start,end-1)))
         else: res.append((u'אלי','IN',(start,end))) # TODO: verify IN!
      elif m==u'על' and anal[-1][1].startswith('S_PRN') and u'עלי' in tok:
         if tok==u'עלי': res.append((u'על','IN',(start,end-1)))
         else: res.append(('ELI','IN',(start,end))) # TODO: verify IN!
      elif m==u'את' and anal[-1][1].startswith('S_PRN') and u'אות' in tok:
         res.append((u'אות','AT',(start,end)))
      elif m==u'בין' and anal[-1][1].startswith('S_PRN') and u'ביני' in tok:
         res.append((u'ביני','IN',(start,end))) #TODO: verify IN!
      else:
         res.append((m,p,(start,end)))
   return tuple(res)

class Analyzer:
   def __init__(self, always_include_full=False):
      self.analyzer = kcp.create_tb_compatible_kc_based_analyzer()
      self.always_include_full = always_include_full

   def _get_segmentations(self,tok):
      anals = self.analyzer.get_anals(tok)
      anals = (list(anals))
      if not anals:
         #yield ['UNK']
         yield None
         return
      for anal in anals:
         m_ps = anal.get_anal()
         forms = _get_raw_tb_morphs_from_anal(tok,m_ps)
         yield forms
      if self.always_include_full: yield (tok,)

   def get_segmentations(self,tok):
      return set(self._get_segmentations(tok))

def verify_and_fix(forms):
   """
   This fixes some previous bugs in creation of the forms, specfically in creation of
   the numbering, which result in bad position indicators, such as:
    ((u'\u05d0\u05dc', 'IN', (0, 1)), (u'\u05d0\u05e0\u05d9', u'S_PRN-MF-S-1', (2, 3)))
   in which we'd expect either (0,1), (1,3)  or (0,2), (2,3), but not the (0,1), (2,3) we 
   see here, or
     ((u'\u05d1', u'IN', (0, 0)), (u'\u05d4\u05df', u'S_PRN-F-P-3', (0, 2)))
   where (0,0) is bad
   """
   current_end = forms[0][2][0] # [(morph, pos, (*S*,e)),...]
   res = []
   for (morph, pos, (s,e)) in forms:
      if s != current_end:
         s = current_end
      if s >= e:
         e = s+1
      current_end = e
      res.append([morph,pos,(s,e)])
   return res

class AnalyzerWpos:
   def __init__(self, always_include_full=False):
      self.analyzer = kcp.create_tb_compatible_kc_based_analyzer("dummylex",PREFLEX_FILE,debug=True)
      self.always_include_full = always_include_full

   def _get_segmentations(self,tok):
      anals = self.analyzer.get_anals(tok)
      anals = list(anals)
      if not anals:
         #yield ['UNK']
         yield None
         return
      res = []
      for anal in anals:
         #print "anal is:",unicode(anal).encode("utf8")
         m_ps = anal.get_anal_wparts()
         forms = _get_raw_tb_morphs_and_kc_pos_from_anal(tok,m_ps)
         m,p,w = forms[0]
         # trying to get rid of the stupid BXWKMH/to-infinitve+suffix analysis
         if m[0]==u'ב' and 'INFINITIVE' in p:
            continue
         res.append(verify_and_fix(forms))
      # verify_and_fix may have changed the end of some forms. Fix it
      # so that all forms share an end.
      ends   = [forms[-1][2][1] for forms in res]
      if len(set(ends)) > 1:
         end = max(ends)
         for forms in res:
            forms[-1][2] = (forms[-1][2][0],end)
      for forms in res: yield tuple([tuple(x) for x in forms])
      if self.always_include_full: yield (tok,)

   def get_segmentations(self,tok):
      return set(self._get_segmentations(tok))
      
if __name__=='__main__':
   import sys
   analyzer = AnalyzerWpos()
   kca = analyzer.analyzer

   for seg in analyzer.get_segmentations("BHCBEH"):
      print seg
   print
   sys.exit()
   for seg in analyzer.get_segmentations("LBDW"):
      print seg
   print

   print list(kca.get_anals('KFHM'))[0].get_anal_wparts()
   for anal in kca.get_anals('LPIH'):
      print anal.get_anal_wparts()
   for anal in kca.get_anals('WMAXRIHM'):
      print anal.get_anal_wparts()
   print
   for anal in kca.get_anals('HM'):
      print anal.get_anal_wparts()
   print
   print analyzer.get_segmentations("HM")
   print
   print analyzer.get_segmentations("KFHM")
   for seg in analyzer.get_segmentations("KFHM"):
      print seg
   print 
   print "BW",analyzer.get_segmentations("BW")
   print
   print analyzer.get_segmentations("LPIH")
   print analyzer.get_segmentations("WMAXRIHM")
   print analyzer.get_segmentations("KZIT")
   print analyzer.get_segmentations("WUKZIT")
   print analyzer.get_segmentations("WMHBIT")
   print analyzer.get_segmentations("HBIT")
#import sys
#sys.path.append("BguLex/bin")
#from bgulex import BGULex
#bgulex = BGULex()
#print list(bgulex.get_anals("MFLHM"))
#a = Analyzer()
#print list(a.get_segmentations("KFHM"))
