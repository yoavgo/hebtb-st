## Copyright 2010,2011 Yoav Goldberg
##
## This file is part of HebrewConstituencyParser
##
##    HebrewConstituencyParser is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    HebrewConstituencyParser is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with HebrewConstituencyParser.  If not, see <http://www.gnu.org/licenses/>.

def _tokenizer(stream):
   stack = []
   for item in stream:
      if item in ["(",")"]:
         if stack: yield "".join(stack)
         stack = []
         yield item
      elif item in [' ',"\n","\r","\t"]: 
         if stack: yield "".join(stack)
         stack = []
      else:
         stack.append(item)
   if stack: yield "".join(stack)

def sexprs_reader(stream):
   #print "entering sexpr_reader"
   current = []
   for next in stream: 
      if next == '(':
         current.append(sexprs_reader(stream))
      elif next == ')':
         #print "leaving sexpr_reader",current
         return current
      else: # next is a symbol
         current.append(next)
   #print "at_end"
   return current

def sexpr_reader(stream):
   """
   kept only for backward compatability.
   """
   return sexprs_reader(stream)[0]

def read(stream):
   return sexpr_reader(_tokenizer(stream))

def read_as_stream(stream):
   for sexp in sexprs_reader(_tokenizer(stream)):
      yield sexp

def to_string(sexpr):
   if isinstance(sexpr,list): 
      return "( %s )" % " ".join([to_string(sxp) for sxp in sexpr])
   else:
      return sexpr
   
