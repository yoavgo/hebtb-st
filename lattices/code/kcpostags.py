# coding:utf8
## Copyright 2010,2011 Yoav Goldberg
##
## This file is part of HebrewConstituencyParser
##
##    HebrewConstituencyParser is free software: you can redistribute it and/or modify
##    it under the terms of the GNU General Public License as published by
##    the Free Software Foundation, either version 3 of the License, or
##    (at your option) any later version.
##
##    HebrewConstituencyParser is distributed in the hope that it will be useful,
##    but WITHOUT ANY WARRANTY; without even the implied warranty of
##    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##    GNU General Public License for more details.
##
##    You should have received a copy of the GNU General Public License
##    along with HebrewConstituencyParser.  If not, see <http://www.gnu.org/licenses/>.

import os.path
import sys; sys.path.append(os.path.join(os.path.dirname(__file__),"../lexicon/","BguLex2utf8/bin"))
import re

__PREF_PRECEDENCE = {#{{{
      'CONJ' : 0,
      'REL-SUBCONJ' : 1,
      'TEMP-SUBCONJ' : 1,
      'REL' : 1,
      'PREPOSITION' : 2,
      'ADVERB' : 3,
      'DEF': 4,
      }
#}}}

def order_prefs(prefs):#{{{
   prefs = [x for x in prefs if x]
   prefs = sorted(prefs, key=lambda p:__PREF_PRECEDENCE[p])
   return prefs
#}}}

## _kc string functions functions#{{{

def kc_pref_string(prefs):
   return "+".join(sorted(prefs))

def kc_base_string(tag,feats):
   f = [x for x in feats if x]
   if f:
      return "%s-%s" %(tag,"-".join(feats))
   else:
      return tag

def kc_suff_string(suf):
   if not suf:
      return ''
   #type,g,n,p = suf
   return "-".join(suf)

#}}}

def parse_kc_tag(kct):#{{{
   tag = {}
   if kct.find("^U^") > -1:
      pre,kct = kct.split("^U^")
      tag['prequote'] = pre.split(":")[0]
   prf,base,suf = kct.split(":")
   
   prfs = prf.split("+")
   tag['prfs'] = prfs
   
   try: bpos, bfeats = base.split("-",1)
   except ValueError: bpos, bfeats = base,''
   bfeats = bfeats.split("-")
   tag['pos'] = bpos
   tag['feats'] = bfeats

   tag['suf'] = suf
   tag['stype'] = suf.split("-")[0]
   tag['sfeats'] = suf.split("-")[1:]

   return tag
#}}}

def parsed_kc_to_str(pkct): #{{{
   if 'prequote' in pkct:
      tag = [pkct['prequote'], "::", "^U^"]
   else: tag = []
   tag.append("+".join(pkct['pref']))
   tag.append(":")
   tag.append(pkct['pos'])
   if pkct['feats']:
      tag.append("-"+("-".join(pkct['feats'])))
   tag.append(":")
   tag.append("-".join([pkct['stype'],pkct['sfeats']]))
 
   return "".join(tag)

#}}}


####
# Meni's suffix -> string
SUFF_TO_STR = { #{{{
      'S_PP-MF-S-1': u'י',
      'S_AP-MF-S-1': u'י',
      'S_NP-MF-S-1': u'י',
      'S_PRN-MF-S-1': u'י',
      'S_ANP-MF-S-1': u'י',

      'S_PP-MF-S-2': u'כ',
      'S_PP-M-S-2':  u'כ',
      'S_PP-F-S-2':  u'כ',
      'S_PRN-MF-S-2': u'כ',
      'S_PRN-M-S-2': u'כ',
      'S_PRN-F-S-2': u'כ',
        
      'S_AP-MF-S-2': u'כ',
      'S_AP-M-S-2': u'כ',
      'S_AP-F-S-2': u'כ',
        
      'S_NP-MF-S-2': u'כ',
      'S_NP-M-S-2': u'כ',
      'S_NP-F-S-2': u'כ',

      'S_ANP-MF-S-2': u'כ',
      'S_ANP-M-S-2': u'כ',
      'S_ANP-F-S-2': u'כ',
        
      'S_PP-M-S-3': u'ו',
      'S_PRN-M-S-3': u'ו',
      'S_AP-M-S-3': u'ו',
      'S_NP-M-S-3': u'ו',
      'S_ANP-M-S-3': u'ו',

      'S_PP-F-S-3': u'ה',
      'S_PRN-F-S-3': u'ה',
      'S_AP-F-S-3': u'ה',
      'S_NP-F-S-3': u'ה',
      'S_ANP-F-S-3': u'ה',

      'S_PP-MF-P-1': u'נו',
      'S_PRN-MF-P-1': u'נו',
      'S_AP-MF-P-1': u'נו',
      'S_NP-MF-P-1': u'נו',
      'S_ANP-MF-P-1': u'נו',

      'S_NP-M-P-1': u'נו',
      'S_NP-F-P-1': u'נו',

      'S_PP-M-P-2': u'כם',
      'S_PRN-M-P-2': u'כם',
      'S_AP-M-P-2': u'כם',
      'S_NP-M-P-2': u'כם',
      'S_ANP-M-P-2': u'כם',

      'S_PP-F-P-2': u'כן',
      'S_PRN-F-P-2': u'כן',
      'S_AP-F-P-2': u'כן',
      'S_NP-F-P-2': u'כן',
      'S_ANP-F-P-2': u'כן',

      'S_PP-M-P-3': {'S':u'ם', 'P':u'הם', 'DP':u'הם'},  # is base is Singular/Plural?

      'S_PRN-M-P-3': u'הם',
      'S_PRN-MF-P-3': u'הם',

      'S_ANP-M-P-3': u'ם',
      'S_AN-M-P-3': u'ם',
      'S_ANP-M-P-3': u'ם',
      'S_PP-F-P-3': {'S':u'ן', 'P':u'הן', 'DP':u'הן' },

      'S_PRN-F-P-3': u'הן',

      'S_AP-F-P-3': u'ן',
      'S_NP-F-P-3': u'ן',
      'S_ANP-F-P-3': u'ן',
}#}}}
SUFF_TO_PRON = { #{{{
      'S_PP-MF-S-1': u'י',
      'S_AP-MF-S-1': u'י',
      'S_NP-MF-S-1': u'י',
      'S_PRN-MF-S-1': u'י',
      'S_ANP-MF-S-1': u'י',

      'S_PP-MF-S-2': u'כ',
      'S_PP-M-S-2':  u'כ',
      'S_PP-F-S-2':  u'כ',
      'S_PRN-MF-S-2': u'כ',
      'S_PRN-M-S-2': u'כ',
      'S_PRN-F-S-2': u'כ',
        
      'S_AP-MF-S-2': u'כ',
      'S_AP-M-S-2': u'כ',
      'S_AP-F-S-2': u'כ',
        
      'S_NP-MF-S-2': u'כ',
      'S_NP-M-S-2': u'כ',
      'S_NP-F-S-2': u'כ',

      'S_ANP-MF-S-2': u'כ',
      'S_ANP-M-S-2': u'כ',
      'S_ANP-F-S-2': u'כ',
        
      'S_PP-M-S-3': u'ו',
      'S_PRN-M-S-3': u'ו',
      'S_AP-M-S-3': u'ו',
      'S_NP-M-S-3': u'ו',
      'S_ANP-M-S-3': u'ו',

      'S_PP-F-S-3': u'ה',
      'S_PRN-F-S-3': u'ה',
      'S_AP-F-S-3': u'ה',
      'S_NP-F-S-3': u'ה',
      'S_ANP-F-S-3': u'ה',

      'S_PP-MF-P-1': u'נו',
      'S_PRN-MF-P-1': u'נו',
      'S_AP-MF-P-1': u'נו',
      'S_NP-MF-P-1': u'נו',
      'S_ANP-MF-P-1': u'נו',

      'S_NP-M-P-1': u'נו',
      'S_NP-F-P-1': u'נו',

      'S_PP-M-P-2': u'כם',
      'S_PRN-M-P-2': u'כם',
      'S_AP-M-P-2': u'כם',
      'S_NP-M-P-2': u'כם',
      'S_ANP-M-P-2': u'כם',

      'S_PP-F-P-2': u'כן',
      'S_PRN-F-P-2': u'כן',
      'S_AP-F-P-2': u'כן',
      'S_NP-F-P-2': u'כן',
      'S_ANP-F-P-2': u'כן',

      'S_PP-M-P-3': {'S':u'ם', 'P':u'הם', 'DP':u'הם'},  # is base is Singular/Plural?

      'S_PRN-M-P-3': u'הם',
      'S_PRN-MF-P-3': u'הם',

      'S_ANP-M-P-3': u'ם',
      'S_AN-M-P-3': u'ם',
      'S_ANP-M-P-3': u'ם',
      'S_PP-F-P-3': {'S':u'הן', 'P':u'הן', 'DP':u'הן' },

      'S_PRN-F-P-3': u'הן',

      'S_AP-F-P-3': u'ן',
      'S_NP-F-P-3': u'ן',
      'S_ANP-F-P-3': u'ן',
} #}}}
def is_number(lemma):
   return lemma=='_ORTO_'
   if re.match(u"\d+$",lemma): return True
   if re.match(u"\d+[,\.:\-\d]+$",lemma): return True
   return False 
def number_symbol(lemma):
   #return lemma
   return "###NUMBER###"

class KCToken:#{{{
   def _parse_kcanal(self, token, anal, lem):#{{{
      # check pre-token quote
      if anal.find("^U^") > -1:
         pre_anal, anal = anal.split(u"^U^")
         pre_tok ,token = token.split(u'"',1)
         pre_lem ,lem   = lem.split(u':":',1)
      else:
         pre_anal = ":::"
         pre_tok  = ""
         pre_lem  = ""

      self.pre_tok = pre_tok
      self.pre_pos = pre_anal.split(":")[0]
      self.pre_lem = [x for x in pre_lem.split("^") if x]

      self.pre_pos = order_prefs(self.pre_pos.split("+"))

      self.token = token
      self.anal  = anal

      try:
         prf_lemma, lemma = lem.split(":")
      except ValueError:
         prf_lemma, lemma = "", lem

      self.prefs, self.base, self.suff = anal.split(":")

      self.base_pos     =self.base.split('-')[0]
      self.base_features=self.base.split('-')[1:]
      self.suff_type    =self.suff.split("-")[0]
      self.suff_features=self.suff.split("-")[1:]

      self.prefixes     =order_prefs(self.prefs.split("+"))

      self.lemma = lemma
      self.pref_lemmas = [x for x in prf_lemma.split("^") if x]
#}}}

   def __init__(self, token, anal, lem, DEF_AS_PREFIX=True):#{{{
      # keep originals
      self._otoken = token
      self._oanal  = anal
      self._olem   = lem
      
      # parse the damn tag and lemma
      self._parse_kcanal(token, anal, lem)

      self.base_str = None
      self.pref_strs= None
      self.suff_str = None

      self.DEF_AS_PREFIX=DEF_AS_PREFIX

      #}}}

   def __repr__(self):
      return "<KCToken: %s %s>" % (self._otoken, self._oanal)

   def is_definite(self):
      return 'DEF' in self.prefixes

   ## FORM accessors#{{{
   def get_suff_form(self):#{{{
      if self.suff_str!=None: return self.suff_str
      if self.suff:
         form = SUFF_TO_STR[self.suff]
         if isinstance(form, unicode):
            self.suff_str = form
            return form
         else:
            features = self.base_features
            if   'DP' in features: self.suff_str = form['DP']
            elif 'P'  in features: self.suff_str = form['P']
            elif 'S'  in features: self.suff_str = form['S']
            else:
               assert(False),"can't get suffix of %s / %s" % (self.token, self.anal)
            try:
               assert(self._otoken.endswith(self.suff_str)),(self._otoken, self.suff_str)
            except AssertionError, e:
               print >> sys.stderr,"assertion error",e
            return self.suff_str
      else: # no suffix --> no string
         self.suff_str = ''
         return self.suff_str#}}}


   def __get_base_str(self): # Meni's internal function#{{{
      lemma = self.get_lemma()
      if is_number(lemma):  # stubs for now TODO @@
         #print "NUM Lemma",self.get_lemma()
         return number_symbol(lemma)
      
      if self.suff_type == 'S_PRN': return lemma

      # else, we don't use the lemma, but get stuff from the token
      prfs = "".join([x for x in self.get_pref_forms() if x != 'DUMMY_H'])
      suff = self.get_suff_form()

      h = ""
      if 'DEF' in self.prefixes: h =u"ה"
      if len(suff) > 0:
         base_form = self.token[len(prfs):-len(suff)]
         if base_form[0] != u'ה': base_form=h+base_form
         else: base_form=h+base_form   # this looks dumb, but probably very important (try HHCBEH)
         return base_form
      else:
         base_form = self.token[len(prfs):]
         if base_form[0] != u'ה': base_form=h+base_form
         else: base_form=h+base_form   # this looks dumb, but probably very important (try HHCBEH)
         return base_form
      #}}}
      
   def get_base_form(self):#{{{
      if self.base_str != None: return self.base_str
      
      else:
        base = self.__get_base_str()
        if self.suff_type == 'S_PRN': return base 
        
        ret = base;
        pref_strs = self.get_pref_forms()
        if not self.DEF_AS_PREFIX: # this part is really not debugged @@@
           if   ('DEF' in self.prefixes and pref_strs and pref_strs[-1] in [u'ב',u'כ',u'ל']):
              ret = u"ה" + ret;
           elif ('DEF' in self.prefixes):
              ret = u"ה" + ret
        else:
           #if  ('DEF' in self.prefixes and len(pref_strs) > 1 and pref_strs[-2] in ['B','K','L']):
           #   ret = "H" + ret;
           #if 'DEF' in self.prefixes: ret = "H"+ret
           pass

        #assert(len(ret)>0),"ret: %s %s %s " % (ret,self.token,self.base) 
        if not ret: print "no ret: %s %s" % (self.token, self.prefixes)
        if ret and ret[-1] == u'י' and self.suff.startswith('S_PP-F-P'):
           ret=ret[:-1]

        return ret#}}}
         
   def get_pref_forms(self):#{{{
      if self.pref_strs != None: return self.pref_strs

      prefs = self.pref_lemmas
      if (not self.DEF_AS_PREFIX) and ('DEF' in self.prefixes):
         prefs = [x for x in prefs if x != u'ה']
         self.pref_strs = prefs
         return prefs
      #if (self.DEF_AS_PREFIX) and self.prefixes and self.prefixes[-1] == 'DEF' and ((not prefs) or prefs[-1] != 'H'):
      #   prefs.append("DUMMY_H")
      elif self.DEF_AS_PREFIX:
         if (not prefs) and 'DEF' in self.prefixes:
            # the prefix lemma does not have the 'H', but the analysis is definite:
            # (this happens because in the lexicon, the definiteness is not a prefix..)
            prefs.append(u'ה')
         ret_prefs = []
         lastidx=0
         for prev,p in zip(['_']+self.pref_lemmas,self.pref_lemmas):
            if p == u'ה' and ('REL' not in self.prefixes) and prev in [u'כ',u'ב',u'ל']: # H after K,B,L is hidden
               ret_prefs.append('DUMMY_H')
            else: 
               ret_prefs.append(p)
         self.pref_strs = ret_prefs
         return self.pref_strs
      else:
         assert(False),"should not get here"
   #}}}

   def get_pretoken_forms(self):#{{{
      if self.pre_lem:
         return self.pre_lem + ["U"]
      return []
   #}}}
#}}}

   def calc_indices(self):
      pret = "".join(self.get_pretoken_forms())
      prfs = "".join([x for x in self.get_pref_forms() if x != 'DUMMY_H'])
      suff = self.get_suff_form()

      last=0
      self.pret_idxs=[]  # pretoken 
      for p in pret: 
         self.pret_idxs.append((last,last+len(p)))
         last+=len(p)

      pref_idxs=[] # prefixes
      for prev,p in zip([u'_']+self.pref_lemmas,self.pref_lemmas):
         if p == u'ה' and ('REL' not in self.prefixes) and prev in [u'כ',u'ב',u'ל']: # H after K,B,L is hidden
            last_pair = pref_idxs[-1]
            pref_idxs[-1] = (last_pair[0],last_pair[1]-0.5)
            pref_idxs.append((last-0.5,last))
         else: 
            pref_idxs.append((last,last+len(p)))
            last+=len(p)
      self.pref_idxs=pref_idxs

      # NOTE: there's a problem here with "long" suffixes: BN -> B HN
      self.base_idx=(len(pret)+len(prfs),len(self.token)-len(suff))
      self.suff_idx=(len(self.token)-len(suff),len(self.token)) if suff else None

   def get_lemma(self):#{{{
      return self.lemma
   #}}}

   ## POS accessrs#{{{

   def get_pretoken_poses(self):#{{{
      if self.pre_pos:
         return self.pre_pos + ["yyQUOT"]
      else:
         return []
   #}}}

   def get_pref_poses(self):#{{{
      if self.DEF_AS_PREFIX:
         prefs = []
         for x in self.prefixes:
            if x == 'DEF': prefs.append("DEF")
            else: prefs.append(x)
         return prefs
      else:
         return [x for x in self.prefixes if x != 'DEF']
   #}}}

   def get_base_pos(self):#{{{
      if self.DEF_AS_PREFIX:
         if 'DEF' in self.prefixes: 
            return "DEF:"+self.base
         return self.base
      else:
         assert False,"getting base not supported when DEF_AS_PREFIX is false"
   #}}}

   def get_suff_pos(self):#{{{
      return self.suff
   #}}}
#}}}

   def as_morph_seqs(self):
      return self.get_pretoken_forms(), self.get_pref_forms(), [self.get_base_form()], [self.get_suff_form()]

   def as_pos_seqs(self):
      return self.get_pretoken_poses(), self.get_pref_poses(), [self.get_base_pos()], [self.get_suff_pos()]

#}}}

class KCLexiconBasedAnalyzer:#{{{
   def __init__(self, bgulexicon, anal_factory=None):
      self.lex = bgulexicon
      self.anal_factory = anal_factory

   def get_anals(self, token, _tag=None):
      for (tag, lem) in self.lex.get_anals_lemmas(token):
         if _tag and tag != _tag: continue
     
         if self.anal_factory:
            yield self.anal_factory(KCToken(token, tag, lem))
         else: 
            yield KCToken(token, tag, lem)
#}}}

class TBCompatibleAnal:#{{{
   def __init__(self, kctoken):
      self.kctoken = kctoken

   def __repr__(self):
      return ("<TBCompatibleAnal %s>" % (self.kctoken)).encode("utf8")
   @classmethod 
   def from_kctoken(cls, kctoken):
      return cls(kctoken)

   def get_pref_idxs(self):
      return self.kctoken.pref_idxs

   def get_base_idx(self):
      kcs = self.kctoken
      if kcs.token in [u'ככזה',u'ברובה',u'לבדו',u'לאיטה']:
         if kcs.suff_idx is not None:
            return (kcs.base_idx[0],kcs.suff_idx[1])
         else:
            return kcs.base_idx
      if kcs.suff_type == 'S_PP':
         return (kcs.base_idx[0],kcs.suff_idx[1]) 
      return self.kctoken.base_idx

   def get_suff_idxes(self):
      kcs = self.kctoken
      # possesive suffixes are part of the word
      if kcs.suff_type == 'S_PP': return [] # handled in base
      elif kcs.suff_type == 'S_ANP':
         # (DUMMY_AT, REAL_SUF), where DUMMY_AT takes a half
         return [(kcs.suff_idx[0],kcs.suff_idx[1]-0.5), (kcs.suff_idx[1]-0.5,kcs.suff_idx[1])]
      elif kcs.suff_type == 'S_PRN':
         # 2 special cases
         if kcs.token in [u'לבדו',u'לאיטה']: return []
         return [kcs.suff_idx]
      else:
         return []

   def get_pretoken_idxes(self):
      return self.kctoken.pret_idxs

   def get_pretoken_poses(self):#{{{
      return self.kctoken.get_pretoken_poses()
#}}}
   def get_pref_poses(self):#{{{
      return self.kctoken.get_pref_poses()
#}}}
   def get_base_poses(self):#{{{
      kcs = self.kctoken
      if kcs.token in [u'ככזה',u'ברובה',u'לבדו',u'לאיטה']:
         return [kcs.get_base_pos()]
      if kcs.suff_type == 'S_PP':
         return [kcs.get_base_pos() +"/"+ kcs.get_suff_pos()]
      return [kcs.get_base_pos()]
#}}}
   def get_suff_poses(self):#{{{
      kcs = self.kctoken
      # possesive suffixes are part of the word
      if kcs.suff_type == 'S_PP': return [] # handled in base
      #if kcs.suff_type == 'S_PP': return [kcs.get_suff_pos()] # handled in base
      elif kcs.suff_type == 'S_ANP':
         return ['DUMMY_AT',kcs.get_suff_pos()]
      elif kcs.suff_type == 'S_PRN':
         # 2 special cases
         if kcs.token in [u'לבדו',u'לאיטה']: return []
         return [kcs.get_suff_pos()]
      else:
         return []
#}}}

   def get_pretoken_forms(self):#{{{
      return self.kctoken.get_pretoken_forms()
#}}}
   def get_pref_forms(self):#{{{
      return self.kctoken.get_pref_forms()
#}}}
   def get_base_forms(self):#{{{
      kcs = self.kctoken
      if kcs.suff_type == 'S_PP':
         return [kcs.get_base_form() + "/" + kcs.get_suff_form()]
      return [kcs.get_base_form()]
#}}}
   def get_suff_forms(self):#{{{
      kcs = self.kctoken
      # possesive suffixes are part of the word
      if kcs.suff_type == 'S_PP': return [] # handled in base
      #if kcs.suff_type == 'S_PP': return [kcs.get_suff_form()] # handled in base
      elif kcs.suff_type == 'S_ANP':
         return ['_AT_',kcs.get_suff_form()]
      elif kcs.suff_type == 'S_PRN':
         # 2 special cases
         if kcs.token in [u'לבדו',u'לאיטה']: return []
         return [kcs.get_suff_form()]
      else:
         return []
#}}}

   def get_morphs(self):#{{{
      assert(self.kctoken.DEF_AS_PREFIX == True)
      res = self.get_pretoken_forms() + self.get_pref_forms() + self.get_base_forms() + self.get_suff_forms()
      res = [x for x in res if x]
      return res
#}}}
   def get_poses(self):#{{{
      assert(self.kctoken.DEF_AS_PREFIX == True)
      res = self.get_pretoken_poses() + self.get_pref_poses() + self.get_base_poses() + self.get_suff_poses()
      res = [x for x in res if x]
      return res
#}}}
   def get_partnames(self):#{{{
      assert(self.kctoken.DEF_AS_PREFIX == True)
      #res = ['PRET' for x in self.get_pretoken_poses()] + ['PREF' for x in self.get_pref_poses()] + ['BASE' for x in self.get_base_poses()] + ['SUFF' for x in self.get_suff_poses()]
      self.kctoken.calc_indices()
      res = self.get_pretoken_idxes() + self.get_pref_idxs() + [x for x in [self.get_base_idx()]]+ self.get_suff_idxes()  ##TODO  need to move from kctoken to self.  and handle the problem with nonminal-suffixes.
      res = [x for x in res if x]
      return res
#}}}

   def get_anal(self):#{{{
      assert (len(self.get_morphs()) == len(self.get_poses())),"diff length: [%s] [%s]" % (self.get_morphs(), self.get_poses())
      return zip(self.get_morphs(), self.get_poses())
#}}}

   def get_anal_wparts(self):#{{{
      '''
      like get_anal, but, for each part returns also it's origin (PRETtoken PREFix, BASE, SUFFix )
      '''
      morphs = self.get_morphs()
      poses = self.get_poses()
      partnames = self.get_partnames()
      if len(poses) < len(morphs): #NOTE: ugly hack to hide a bug.
         while len(poses) < len(morphs): poses.append(poses[-1]) 
      try:
         assert (len(morphs) == len(poses)==len(partnames)),"diff length: [%s] [%s] [%s]" % (self.get_morphs(), self.get_poses(), self.get_partnames())
      except AssertionError,e: print >> sys.stderr, "failed assertion:",e
      #print "returning",self.get_poses(),self.get_morphs()
      #print "poses:",list(self.get_morphs())
      return zip(morphs, poses, partnames)
#}}}
#}}}


def create_tb_compatible_kc_based_analyzer(bgulex_filename=None, bgupreflex_filename=None,use_menilex=False,debug=False):#{{{
   """
   the returned analyzer has a "get_anals(token,[tag])" method, returning a list
   of "Anal" objects, each of which has a "get_anal()" method. Anal.get_anal() returns a list of morph-pos pairs:
   [(m1,ps),(m2,p2),...]
   """
   import bgulex
   if True:
      lex = bgulex.SQBackedBGULex()
   elif bgulex_filename: 
      assert bgupreflex_filename != None
      lex = bgulex.BGULex(bgulex_filename, bgupreflex_filename, debug=debug)
   elif use_menilex:
      import menilex
      lex = menilex.MENILex()
   else:
      lex = bgulex.BGULex()
   return KCLexiconBasedAnalyzer(lex, TBCompatibleAnal.from_kctoken)
#}}}

if __name__ == '__main__':
   a = create_tb_compatible_kc_based_analyzer()
   print "@",a.lex.get_anals("BBITW")
   anals = list(a.get_anals("BBITW"))
   print anals[0].get_anal()
   anals = list(a.get_anals("B50"))
   for anal in anals:
      print anal.get_anal()


