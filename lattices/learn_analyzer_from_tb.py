import sys
from collections import defaultdict
data = []
wc = defaultdict(int)
for line in sys.stdin:
   line = line.strip().split()
   if not line: continue
   w = line[1]
   p = line[4]
   m = line[-5]
   data.append((w,p,m))
   wc[w] += 1

words = defaultdict(lambda:defaultdict(int))
for w,p,m in data:
   if wc[w] < 3:
      words['UNK'][(p,m)] += 1
      words['UNK_'+w[-1:]][(p,m)] += 1
      words['UNK_'+w[-2:]][(p,m)] += 1
   if wc[w] > 10:
      words[w][(p,m)] += 1

for w,d in words.iteritems():
   if sum(d.values()) < 30: continue
   print w,
   for (p,m),c in sorted(d.iteritems(),key=lambda ((p,m),c):-c):
      if c > 5: print ":".join([p,m]),
   print



