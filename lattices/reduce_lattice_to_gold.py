import sys
sys.path.append("../bin")
from pio import io

def read_lat(fh):
   lat = []
   for line in fh:
      line = line.strip().split()
      if not line:
         yield lat
         lat = []
      else:
         lat.append(line)
   if lat: yield lat

def group_conll_by_tok(sent):
   prev = None
   s = []
   for tok in sent:
      tid = str(tok['id']).split(".")[0]
      if tid != prev:
         if s: yield s
         s = []
      s.append(tok)
      prev = tid
   if s: yield s

def group_lat_by_tok(lat):
   prev = None
   s = []
   for tok in lat:
      tid = tok[-1]
      if tid != prev:
         if s: yield s
         s = []
      s.append(tok)
      prev = tid
   if s: yield s


def match(dep_pos, lat_poses):
   dep_pos = dep_pos.replace("N_S","N/S")
   if dep_pos in lat_poses:
      return dep_pos
   elif dep_pos.endswith(":") and dep_pos[:-1] in [x.split(":")[0] for x in lat_poses]:
      return dep_pos
   elif dep_pos == "IN:" and "PREPOSITION:" in lat_poses:
      return dep_pos
   else:
      return None

for lat,gold in zip(read_lat(file(sys.argv[1])),io.conll_to_sents(file(sys.argv[2]))):
   lbt= list(group_lat_by_tok(lat))
   sbt= list(group_conll_by_tok(gold))
   assert(len(lbt) == len(sbt))
   for ltoks,stoks in zip(lbt,sbt):
      for s in stoks:
         form = s['form']
         pos = s['tag'] + ":" + "|".join(s['morph'])
         if pos.endswith(":_"): pos = pos[:-1]
         #for t in ltoks:
         #   print t
         #   print t[3]
         try:
            lforms = [t[2] for t in ltoks]
            lposes = [t[3] for t in ltoks]
         except IndexError:
            print "WTF"
            continue
         if not form in lforms: print form,lforms
         #if not match(pos,lposes):
         #   print pos,lposes
