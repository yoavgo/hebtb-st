import sys

LAT_FILE = sys.argv[1]
TAGGED_FILE = sys.argv[2]

def tokenize_blanks(fh):
   s = []
   for line in fh:
      line = line.strip().split()
      if not line:
         if s: yield s
         s = []
         continue
      s.append(line)
   if s: yield s

def by_lat_token(lines):
   prev = None
   acc = []
   for line in lines:
      if line[-1] != prev:
         if acc: yield acc
         acc = []
      acc.append(line)
      prev = line[-1]
   if acc: yield acc

def by_tag_token(lines):
   prev = None
   acc = []
   for line in lines:
      tid = line[0].split(".")[0]
      if tid != prev:
         if acc: yield acc
         acc = []
      acc.append(line)
      prev = tid
   if acc: yield acc

for lat,sent in zip(tokenize_blanks(file(LAT_FILE)),tokenize_blanks(file(TAGGED_FILE))):
   for ltok,ttok in zip(by_lat_token(lat),by_tag_token(sent)):
      for item in ltok:
         print item
      print "---"
      for item in ttok:
         print item
      print
