import sys
sys.path.append("lattices/code")
import translate
for line in sys.stdin:
   line = line.decode("utf8").strip().split()
   if not line:
      print
      continue
   if line[1][0] != 'y': line[1] = translate.tb2heb(line[1])
   print "\t".join(line).encode("utf8")
