import sys
for line in sys.stdin:
   line = line.strip()
   if not line:
      print
      continue
   line = line.replace("AWT@suf\t_\tAT\tAT","AWT@suf\t_\tDUMMY_AT\tDUMMY_AT")
   if "\tHKL\t" in line: line = line.replace("DEF@DT","NN")
   line = line.replace("_S_PP","/S_PP")
   print line
