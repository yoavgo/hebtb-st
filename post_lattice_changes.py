import sys
CPOS = 4
FPOS = 5
for line in sys.stdin:
   line = line.strip()
   if not line:
      print
      continue
   line = line.replace("NEG","RB")
   line = line.replace("_S_PP","/S_PP")
   line = line.split()
   if "-" in line[CPOS]:
      p,f = line[CPOS].split("-")
      assert(f in ["TOINFINITIVE","SUBCONJ"]),f
      line[CPOS] = p
   if "/" in line[CPOS]:
      line[CPOS] = line[CPOS].split("/")[0]
   print "\t".join(line)
