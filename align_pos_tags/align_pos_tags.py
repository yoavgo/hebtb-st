#TODO: add HKL as NN to lexicon
import sys
from itertools import izip as zip
sys.path.append("../bin")
import hebtb_reader
from pio import io

const_file = "../new_data/treebanks/consttb.layered"
deps_file = "../new_data/treebanks/deptb"

CONST_OUT = file(const_file + ".unified_pos","w")
DEPS_OUT = file(deps_file + ".unified_pos","w")

def read_const(fh=None):
   if fh == None: fh = file(const_file)
   for i,tree in hebtb_reader.read_one_tree_per_line_as_stream(fh):
      yield tree

def read_deps(fh=None):
   if fh == None: fh = file(deps_file)
   for sent in io.conll_to_sents(fh):
      yield sent

def normalize_tb_tag(ctag):
   if "/" in ctag:
      ctag, suff = ctag.split("/")
      ctag = ctag.split("-")[0]
      suff = suff.split("-")[0]
      return ctag+"_"+suff
   return ctag.split('-')[0]

MISS_LOG = file("miss.log","w")

MOD_WORDS = {
      'FAJ': 'NN',
      'HLA': '??',
      'HRI': '??',
      'ITRH': 'JJ',
      'KDI': 'IN',
      'KI': 'CC',
      'PI': 'RB', #?? PI 3, PI 5,...
      'RB': 'NN', #?? RB SRN...
      'GIAW': 'P',
      'DW': 'P',
      'RH': 'P',
      'PLWS': 'CC',
      }

tag_mappings = { # from (ctag,dtag) to new_tag
      ('IN','PREPOSITION'):'IN',
      ('PREPOSITION','PREPOSITION'):'IN',
      ('ADVERB','ADVERB'):'RB',
      ('!!ZVL!!','!!ZVL!!'):'ZVL',
      } 
for i,(const,deps) in enumerate(zip(read_const(),read_deps())):
   const_yield = list(const.collect_leaves(ignore_empties=True))
   deps_yield = deps
   assert(len(deps_yield) == len(const_yield))
   prev_word = None
   for wid,(cleaf,dleaf) in enumerate(zip(const_yield, deps_yield)):
      if dleaf['form'].endswith("@suf"):
         cleaf.set_word(dleaf['form'])
      assert(cleaf.get_word() == dleaf['form']),(cleaf.get_word(), cleaf.get_pos(), cleaf.parent.get_name(), dleaf['form'])
      ctag = cleaf.get_pos()
      dtag = dleaf['tag']
      ctag = normalize_tb_tag(ctag)
      dtag = dtag.split("-")[0]
      if dtag == 'DUMMY_AT':
         assert(ctag[0] != '!')
         cleaf.set_pos('DUMMY_AT')
      new_tag = None
      if ctag in ['!!UNK!!','!!MISS!!']:
         orig_tb_tag = cleaf.parent.get_name().split("-")[0]
         full_orig_tag = cleaf.parent.get_name()
         if cleaf.get_word() == 'AWT' or (cleaf.get_word() == 'AT' and ctag == '!!UNK!!'):
            new_tag = 'DUMMY_AT'
            dtok = deps_yield[wid+1]
            dtok['tag'] = 'PRP'
            dtok['ctag'] = 'PRP'
            dtok['form'] = dtok['form'] + "@suf"
            ctok = const_yield[wid+1]
            ctok.set_pos('PRP')
         elif orig_tb_tag == 'VB' and '-MD' in full_orig_tag:
            new_tag = 'MD'
         elif orig_tb_tag == 'VB' and '-M' in full_orig_tag:
            new_tag = 'VB-TOINFINITIVE'
         elif orig_tb_tag == 'VB' and full_orig_tag.split("-")[1][-1] in ['V','T']:
            new_tag = 'VB'
         elif orig_tb_tag == 'VB' and full_orig_tag.split("-")[1][-1] == 'H':
            new_tag = 'BN'
         elif orig_tb_tag == 'IN':
            new_tag = 'IN' #TODO: make sure this new_tag is actually used below
         elif cleaf.get_word() == 'B' and orig_tb_tag == 'NNP':
            new_tag = 'NNP'
         elif orig_tb_tag == 'JJ' and cleaf.get_word() in ['KWLW','KWLH']:
            new_tag = '??'
         elif orig_tb_tag == 'PRP' and cleaf.get_word() in ['KN']:
            new_tag = '??'
         elif orig_tb_tag == 'RB' and cleaf.get_word() in ['KW']:
            new_tag = 'RB'
         elif orig_tb_tag == 'CC' and cleaf.get_word() in ['W']:
            new_tag = 'CONJ'
         elif orig_tb_tag == 'H':
            new_tag = 'DEF'
         elif cleaf.get_word() in ['AIZW','AILW']:
            new_tag = 'DTT'
         elif dtag == 'PUNC':
            new_tag = orig_tb_tag
         elif cleaf.get_word() == 'F':
            new_tag = 'REL-SUBCONJ'
         elif cleaf.get_word() == 'H':
            new_tag = 'DEF' # for consistency with the rest. preferably, could be REL-SUBCONJ
         elif cleaf.get_word() == 'IHIH':
            new_tag = 'COP'
         elif cleaf.get_word() == 'FMC': new_tag = 'NN'
         elif cleaf.get_word() == 'HBH': new_tag = 'MD'
         elif cleaf.get_word() == 'KF': new_tag = 'TEMP-SUBCONJ'
         elif cleaf.get_word() == 'MRBH': new_tag = 'DTT'
         elif cleaf.get_word() == 'RBBWT': new_tag = 'CDT'
         elif cleaf.get_word() == 'W': new_tag = 'CONJ'
         elif cleaf.get_word().startswith("yy"): new_tag = cleaf.get_word()
         elif cleaf.get_word() in ['HIW','THIIH']: new_tag = 'COP'
         elif cleaf.get_word() in ['MHWWH','MHWWIM','HWWTH']: new_tag = 'BN'
         elif cleaf.get_word() == 'EWFWT' and orig_tb_tag == 'AUX':
            cleaf.parent.set_name("VB-NRH-DEP_HEAD")
            new_tag = 'BN'
         elif orig_tb_tag == 'MOD':
            if cleaf.get_word() in MOD_WORDS:
               new_tag = MOD_WORDS[cleaf.get_word()]
            elif cleaf.get_word() == 'ITR':
               if cleaf.parent.parent.as_sent().endswith('ITR'):
                  new_tag = 'JJ'
               elif cleaf.parent.parent.as_sent().startswith('ITR'):
                  new_tag = 'JJT'
               else:
                  assert(False)
            elif cleaf.get_word() == 'KLL':
               if cleaf.parent.parent.as_sent() == 'KLL LA':
                  new_tag = 'RB'
               elif cleaf.parent.parent.as_sent().startswith('KLL 081'):
                  new_tag = 'DTT'
               else:
                  assert(False)
            else:
               new_tag = 'RB'
         elif orig_tb_tag == 'MOD':
            new_tag = 'RB'
         #elif dtag == 'RB': new_tag = 'RB'
         elif orig_tb_tag in ['NNP','NN','JJ','NNT','PRP','CC','RB','CD','JJT']:
            new_tag = orig_tb_tag
            print >> MISS_LOG, cleaf.get_word(),new_tag
         else:
            assert(False)
            if True:
               print "SPLIT?",ctag,dtag,cleaf.get_word(),"tb tag:",cleaf.parent.get_name(),cleaf.parent.parent.as_sent()
            else:
               print ctag,dtag,cleaf.get_word(),cleaf.parent.get_name()
      elif ctag == '!!SOME_!!':
         if cleaf.get_word() == 'M': new_tag = 'IN'
         elif cleaf.get_word() == 'W': new_tag = 'CONJ'
         elif cleaf.get_word() == 'H': new_tag = 'DEF'
         elif cleaf.get_word() == 'FUX': new_tag = 'NN'
         elif cleaf.get_word() == 'yyDOT': new_tag = 'yyDOT'
      elif ctag == '!!ZVL!!' or dtag == '!!ZVL!!': new_tag = 'ZVL'
      elif new_tag == None and dtag == ctag and dtag != 'PUNC':
         assert(ctag[0] != '!'),(ctag,new_tag)
         prev_word = cleaf.get_word()
         continue
      elif (ctag,dtag) in tag_mappings:
         new_tag = tag_mappings[(ctag,dtag)]
      else:
         if False: pass
         elif dtag == 'PUNC':
            new_tag = cleaf.parent.get_name()
            #print "changing",cleaf.get_pos(),"to",new_tag
         elif ctag == 'DUMMY_AT':
            new_tag = 'DUMMY_AT'
         elif ctag == 'REL' and dtag == 'DEF': #TODO: look at cases later
            new_tag = 'REL'
         elif i in [7,11,12,13,16] and ctag == 'REL' and dtag == 'DEF':
            new_tag = 'REL'
         #elif i in [55,151,320,386,658] and ctag == 'IN' and dtag == 'AT': #TODO: look at cases later
         elif ctag == 'IN' and dtag == 'AT':
            new_tag = 'IN'
         elif ctag == 'NN' and dtag == 'DEF@DT' and dleaf['form'] == 'HKL':
            #TODO: add HKL to lexicon as NN instead of DEF@DT
            new_tag = 'NN'
         else:   
            print "unknown pair:",i,cleaf.get_word(),cleaf.parent.get_name(),("const:",ctag,"dep:",dtag),[prev_word]," ".join(t['form'] for t in deps_yield)
            sys.exit()
      if new_tag != None:
         cleaf.set_pos(new_tag)
         dleaf['tag'] = new_tag
         dleaf['ctag'] = new_tag
      prev_word = cleaf.get_word()
   print >> CONST_OUT, const.as_sexpr()
   io.out_conll(deps,DEPS_OUT)
         

