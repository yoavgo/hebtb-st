import sys
import hebtb_reader

for i,tree in hebtb_reader.read_one_tree_per_line_as_stream(sys.stdin):
   # fix AT under PP to EM/IN
   leafs = list(tree.collect_leaves(ignore_empties=True))
   for leaf in leafs:
      parent = leaf.parent
      if len(list(parent.collect_leaves())) == 1:
         #print parent.as_sexpr()
         print parent.get_name(), leaf.get_pos()

