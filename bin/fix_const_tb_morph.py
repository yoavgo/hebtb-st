trans = """
Person=1:per=1
Person=2:per=2
Person=3:per=3
Person=A:per=A
Num=PLU:num=P
Num=SING:num=S
Num=BOTH:num=S|num=P
Gen=FEM:gen=F
Gen=MASC:gen=M
Gen=BOTH:gen=M|gen=F
Tense=PRES:tense=BEINONI
Tense=FUT:tense=FUTURE
Tense=IMP:tense=IMPERATIVE
Tense=PAST:tense=PAST
Clitic.Pers=1:suf_per=1
Clitic.Pers=2:suf_per=2
Clitic.Pers=3:suf_per=3
Clitic.Gen=FEM:suf_gen=F
Clitic.Gen=MASC:suf_gen=M
Clitic.Num=PLU:suf_num=P
Clitic.Num=SING:suf_num=S
Def=DEF:def=d
Def=-:def=-
""".strip().split()

trans = [x.split(":") for x in trans]

import sys
for line in sys.stdin:
   for frm,to in trans:
      line = line.replace(frm,to)
   print line.strip()
