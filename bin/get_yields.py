import sys
import hebtb_reader

for i,tree in hebtb_reader.read_one_tree_per_line_as_stream(sys.stdin):
   if 'pos' in sys.argv:
      print " ".join([x.get_pos().split("-")[0].split("_")[0] for x in tree.collect_leaves()])
   else:
      print tree.as_words()
