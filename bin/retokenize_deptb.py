#I removed from deptb: H H HRIM, H H PWELIM, B H H FJXIM, H H IXSIM, H H AMRIQAIM, H H MHLKIM


# re-index the deptb so that token information is consistent with the raw file.
# this primarily means that parts of token-with-a-quote should all share the same token id.
import sys
from pio import io

depf = sys.argv[1] if sys.argv[1:] else "treebanks/deptb.unified_pos.wmorph.fixes"
rawf = sys.argv[2] if sys.argv[2:] else "treebanks/raw"
depsents = list(io.conll_to_sents(file(depf)))
rawsents = list(file(rawf))

def is_num(s):
   return s.replace(",","").replace(".","").isdigit()

PRONS = {
      "LH":["L","HIA@suf"],
      "ATK":["EM","ATH@suf"],
      "ATM":["EM","HM@suf"],
      "AITM":["EM","HM@suf"],
      "ATW":["EM","HWA@suf"],
      "ATH":["EM","HIA@suf"],
      "EIMW":["EM","HWA@suf"],
      "EIMH":["EM","HIA@suf"],
      "WEIMH":["W","EM","HIA@suf"],
      "EIMM":["EM","HM@suf"],
      "FEIMM":["F","EM","HM@suf"],
      "EIMN":["EM","HN@suf"],
      "ATNW":["EM","ANXNW@suf"],
      "LKL":["L","HKL"],
      "BKL":["B","HKL"],
      "LPNI":["LPNI","ANI@suf"],
      "LPNI":["LPNI","ANI"],  #TODO SHOULD NOT BE HERE
      "WBW":["W","B","HWA@suf"],
      "WBH":["W","B","HIA@suf"],
      "BINHN":["BINI","HN@suf"],
      "BINHM":["BINI","HM@suf"],

      "LPWTRH":["LPTWR","AWT@suf","HIA@suf"],
      "LXWQRW":["LXQWR","AWT@suf","HWA@suf"],
      "HHRIM":["H","HRIM"],
      "LDIDI":["LDID","ANI"],

      # special cases for tagger
      # TODO: look at the wierd ones with the numbers.. remove from corpus??
      "BI." : ["B","ANI@suf","yyDOT"],
      "GII." : ["GII","yyDOT"],
      "BI.AII" : ['B', 'ANI@suf', 'yyDOT', 'AII'],
      "LKN": ["L","ATN@suf"],
      "21PLIWN" : ["PLIWN","21","21"],
      "51P6ARBEIM" : ["P6ARBEIM", "51","51"],
      "TX22FAXDIM": ['TX', 'F', 'AXDIM', '22'],
      "F22XT": ['F', 'XT', '22'],
      "71PEL": ['PEL', '71', '71'],
      }


def consume(lst, tok):
   otok = tok
   parts = []
   for i,item in enumerate(lst):
      if not tok: break
      if tok in PRONS:
         candidate = PRONS[tok]
         mismatch = False
         for c,l in zip(candidate,lst):
            if (c != l):
               if c.replace("@suf","") != l: #TODO: try to remove this one. it is a mistake in tb..
                  mismatch = True
         if not mismatch:
            parts = candidate
            tok = ''
            return parts
      if tok.startswith("U") and item == "yyQUOT":
         tok = tok[1:]
         parts.append(item)
      elif item == "AWT@suf":
         if "@" in lst[i+1]:
            parts.append(item)
            parts.append(lst[i+1])
            tok = ''
            return parts
      elif is_num(item) and is_num(tok): #special case. should fix in tbs!
         parts.append(item)
         tok = ''
         return parts
      elif item == 'H':
         if parts and parts[-1] in ['L','B','K']:
            if tok[0] != 'H':
               parts.append(item)
            elif tok[0] == 'H':
               if lst[i+1] == tok:
                  parts.append("H")
                  parts.append(lst[i+1])
                  tok = ''
               else:
                  parts.append("H")
                  tok = tok[1:]
         else:
            if tok.startswith("H"):
               parts.append("H")
               tok = tok[1:]
      elif tok.startswith(item):
         tok = tok[len(item):]
         parts.append(item)
      elif tok.startswith(item.replace("@suf",'')):
         tok = tok[len(item.split("@")[0]):]
         parts.append(item)
         assert (not tok)
      elif tok in ['H','NW','W','K','MK','I','M','KM','IW','N','HW'] and "@" in item:
         parts.append(item)
         tok = ''
      elif item.startswith("yy") and (len(tok) == 1 or tok == '...'):
         tok = ''
         parts.append(item)
      else:
         print "can't align",otok,tok,lst
         sys.exit()
   return parts


def align(raw, splitted):
   raw = iter(raw)
   sindex = 0
   for i, tok in enumerate(raw,1):
      parts = consume(splitted[sindex:], tok)
      sindex += len(parts)
      yield i, tok, parts

if __name__ == '__main__':
   retokenize=True
   dump_quotes=False
   if dump_quotes:
      for sidx,(raw,dep) in enumerate(zip(rawsents, depsents)):
         dep_forms = [t['form'] for t in dep]
         raw_forms = raw.split()
         aligned = list(align(raw_forms, dep_forms))
         new_idxs=[]
         for i, tok, parts in aligned:
            #if 'yyQUOT' in parts:
            if 'U' in tok:
               print tok,parts

   if retokenize:
      for sidx,(raw,dep) in enumerate(zip(rawsents, depsents)):
         dep_forms = [t['form'] for t in dep]
         raw_forms = raw.split()
         aligned = list(align(raw_forms, dep_forms))
         new_idxs=[]
         for i, tok, parts in aligned:
            for pi,p in enumerate(parts):
               new_idxs.append("%s.%s" % (i,pi))
         old_idxs = [t['id'] for t in dep]
         assert(len(new_idxs) == len(old_idxs))
         imap = dict(zip(old_idxs,new_idxs))
         imap[0] = 0
         for tok in dep:
            tok['id'] = imap[tok['id']]
            tok['parent'] = imap[tok['parent']]
         io.out_conll(dep)
         #us = [f for f in raw_forms if 'U' in f]
         #for u in us:
         #print us
