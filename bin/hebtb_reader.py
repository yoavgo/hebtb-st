#!/usr/bin/python25
# encoding: cp1255
import copy
import sys
from pytrees.tree import *
from pytrees import sexprs
from collections import defaultdict

         
class PostagsData:
   def __init__(self, fh, pos_transformer=None):
      """
      pos_transformer: a function for string to string, which will
      be applied on all POStags
      """
      if not pos_transformer:
         pos_transformer = lambda x:x
      self.raw_sents = list(self._read_sentences(fh))
      self.sents = self._parse_sentences(self.raw_sents, pos_transformer)

   def _read_sentences(self,fh):
      cur_num = None
      sent = []
      for line in fh:
         line = line.strip()
         if line.startswith(u'tree') or line.startswith(u'#'): 
            if cur_num != None:
               yield (cur_num, sent)
            cur_num = int(line.split(u"#")[1])
            sent = []
         else:
            if line:
               sent.append(line)
      if sent: yield (cur_num, sent)

   def _parse_sentences(self, raw_sents, pos_transformer):
      sents = dict()
      for (num, sent) in raw_sents:
         parsed = []
         for line in sent:
            if line[0] == u"(":
               line = line.split(u"(")[1:]
               token = u"("
            else:
               line = line.split(u"(")
               token = line[0].strip()
            if token == u"(": token = u"yyLRB"
            if token == u")": token = u"yyRRB"
            poses = [x.strip()[:-1].split() for x in line[1:]]
            poses = [(part,pos_transformer(pos)) for pos,part in poses]

            parsed.append((token, poses))
         sents[num]  = parsed
      return sents

   def sentences(self):
      """
      sentences: a dict of { num : sentence }
         num: an int, the number from postags file (tree #1234)
         sent: a list of the form [ (token, [(part,pos),(part,pos),...]), ...]
      """
      return self.sents

   def apply_to_tokens(self, token_func):
      """
      token_func: func(tok,[(part,pos),(part,pos),...]) -> (tok, [(part,pos),(part,pos)])
      """
      for num, sent in self.sents.iteritems():
         new_sent = []
         for tok, anals in sent:
            new_sent.append(token_func(tok,anals))
         self.sents[num] = new_sent


class PostagsReader:
   def __init__(self, fh, pos_transformer = None):
      self.data = PostagsData(fh, pos_transformer)

   def sentences(self):
      """
      sentences: a dict of { num : sentence }
         num: an int, the number from postags file (tree #1234)
         sent: a list of the form [ (token, [(part,pos),(part,pos),...]), ...]
      """
      return self.data.sentences()

   def sentences_iter(self):
      for num, sent in self.sentences().iteritems(): yield num, sent

   def surface_forms_iter(self):
      """
      a stream of (num , surface_form)
      where surface_form is a list of tokens
      """
      for num, sent in self.sentences_iter():
         yield (num, [tok for (tok,anal) in sent])

   def surface_forms(self):
      """
      return a dict of {num : surface_form}
      where surface_form is a list of tokens
      """
      return dict(self.surface_forms_iter())

   def stream_of_tokens(self):
      """
      each token of the form (token,[(part,pos),(part,pos),...])
      """
      #for num, sent in sorted(self.sents.iteritems(), key=lambda x:x[1]):
      for num, sent in self.sentences_iter():
         for item in sent: yield item

   def lexemes_count(self):
      s = set()
      for tok,parts in self.stream_of_tokens():
         for part,pos in parts:
            s.add(part)
      return len(s)

   def as_lexicon_(self):
      lex = defaultdict(set)
      for tok,parts in self.stream_of_tokens():
         lex[tok].add(tuple(parts))
      return lex

   def write(self, fh):
      for num, sent in self.sentences_iter():
         fh.write( "tree #%s\n" % num )
         for tok, anals in sent:
            anals_string = " ".join(["(%s %s)" % (p,w) for w,p in anals])
            fh.write("%s %s\n" % (tok, anals_string))



class RestrictedPostagsReader(PostagsReader):
   def __init__(self, postags_reader, from_sent, to_sent, max_len=None):
      self.data = postags_reader.data
      self.from_sent = from_sent
      self.to_sent = to_sent
      self.max_len = max_len

   def sentences(self):
      return dict(self.sentences_iter())

   def sentences_iter(self):
      for num, sent in PostagsReader.sentences(self).iteritems():
         if self.max_len and len( sent ) < self.max_len: continue
         if self.from_sent <= num < self.to_sent: yield num, sent


class PredRestrictedPostagsReader(PostagsReader):
   def __init__(self, postags_reader, pred_f, max_len=None):
      """
      pred_f: f(num,sent) -> T/F   (True: keep sentence)
      """
      self.data = postags_reader.data
      self.pred_f = pred_f
      self.max_len = max_len

   def sentences(self):
      return dict(self.sentences_iter())

   def sentences_iter(self):
      for num, sent in PostagsReader.sentences(self).iteritems():
         if self.max_len and len( sent ) < self.max_len: continue
         if self.pred_f(num,sent): yield num, sent


def _heb_tb_trees_yielder(fh):
   """
   yield (tree_number, tree_string)s
   """
   current_number = None
   current_tree = []
   for line in fh:
      if line.startswith("tree #") or line.startswith("tree#"):
         if current_tree: 
            yield (current_number, "\n".join(current_tree))
         current_tree = []
         current_number = int(line.strip().split("#")[1])
      else:
         if line.strip():
            current_tree.append(line.strip())
   if current_tree:
      yield (current_number, "\n".join(current_tree).strip())
   
def read_hebtb_v1_as_stream(fh):
   """
   read HebrewTB v1 file, producing a stream of (num,tree)
   num is an int, from the tb file (tree #1234)
   """
   for (num, tr) in _heb_tb_trees_yielder(fh):
      #print "parsing",tr
      #res[num] = tree.bracket_parse("%s" % tr)
      if isinstance(tr,str): tr = unicode(tr)
      sexpr = (sexprs.read(iter(tr)))
      #print num
      tree = LingTree.from_sexpr(sexpr[0])
      yield (num,tree)

def filtered_read_hebtb_v1_as_stream(fh, from_sent, to_sent):
   """
   read HebrewTB v1 file, producing a stream of (num,tree)
   num is an int, from the tb file (tree #1234)
   """
   #for num, tree in read_hebtb_v1_as_stream(fh):
   for num, tree in read_one_tree_per_line_as_stream(fh):
      if from_sent <= num < to_sent:
         yield num, tree

def pred_filtered_read_hebtb_v1_as_stream(fh, pred_f):
   """
   read HebrewTB v1 file, producing a stream of (num,tree)
   num is an int, from the tb file (tree #1234)
   pred_f: f(num,tree) -> T/F  (True: keep)
   """
   for num, tree in read_one_tree_per_line_as_stream(fh):
      if pred_f(num,tree):
         yield num, tree

def read_hebtb_v1(fh):
   """
   read HebrewTB v1 file, producing a dictionary of trees
   {num:tree}  (num is an int, from the tree number [tree #1234])
   """
   res = dict()
   for (num, tr) in read_hebtb_v1_as_stream(fh):
      #print "parsing",tr
      #res[num] = tree.bracket_parse("%s" % tr)
      res[num] = tr
      #print num
   return res

def read_one_tree_per_line_as_stream(fh,keep_noparse=False):
   """
   produce a stream of (num, tree)
   the numbers are the line number in the file (starting from 1)
   """
   for i,line in enumerate(fh):
      if isinstance(line,str): line = unicode(line)
      if not line.strip(): 
         yield None,None
         continue
      if not line.startswith("("):
         if keep_noparse: 
            yield i,line
            continue
         sys.stderr.write( "Skipping line %s\n" % line.strip() )
         continue
      elif line.startswith("(("):
         tree = LingTree.from_str(line.strip())
      else:
         tree = LingTree.from_str("(%s)" % line.strip())
      yield i,tree

      
#s = sexprs.read((iter(t.strip())))
#dt = LingTree.from_sexpr(s[0])
#l = dt.collect_leaves()
#st = sexprs.to_string(s)

if __name__ == '__main__':
   s = PostagsReader(file("/home/yoavg/Vork/Research/corpora/trees/heb/tbv2gt/tbv2.postags"))
   sents = r.sentences()

