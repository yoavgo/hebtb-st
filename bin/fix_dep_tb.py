# add dummy "AWT" node before HIA@suf S_ANP
import sys
from pio import io

for sent in io.conll_to_sents(sys.stdin):
   # add AWT between VB-INF and S_ANP
   new_toks = []
   mapped = {}
   dont_remap = set()
   for i,tok in enumerate(sent):
      if tok['tag'] == 'S_ANP':
         new_toks.append({
            "id": tok['id'],
            "parent": tok['parent'],
            "prel": "OBJ",
            "form": "AWT@suf",
            "lem": "_",
            "tag": "AT",
            "ctag": "AT",
            "tbtag": "_",
            "morph" : "_",
            "extra:": "_"})
         mapped[tok['id']] = tok['id'] + 0.1
         tok['parent'] = tok['id']
         tok['id'] = tok['id'] + 0.1
         dont_remap.add(tok['id'])
   sent = sent + new_toks
   for tok in sent:
      if tok['id'] in dont_remap: continue
      if tok['parent'] in mapped: tok['parent'] = mapped[tok['parent']]
   sent = sorted(sent, key=lambda t:t['id'])
   #if new_toks: print "##", [t['id'] for t in new_toks]
   io.out_conll(sent)

