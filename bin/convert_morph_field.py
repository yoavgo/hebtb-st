from pio import io
import os.path
HERE = os.path.dirname(__file__)
import sys

items = """
_:_
1:per=1
2:per=2
3:per=3
A:per=A
AT:_
BEINONI:tense=BEINONI
D:num=D
DP:num=D|num=P
F:gen=F
FUTURE:tense=FUTURE
IMPERATIVE:tense=IMPERATIVE
M:gen=M
MF:gen=M|gen=F
SP:num=s|num=P
NEGATIVE:polar=neg
P:num=P
PAST:tense=PAST
POSITIVE:polar=pos
S:num=S
suf_1:suf_per=1
suf_2:suf_per=2
suf_3:suf_per=3
suf_F:suf_gen=F
suf_M:suf_gen=M
suf_MF:suf_gen=M|suf_gen=F
suf_P:suf_num=P
suf_S:suf_num=S
"""
DROP="""
HIFIL
PAAL
NIFAL
HITPAEL
PIEL
PUAL
HUFAL
""".strip().split()
MOVE_TO_TAG="""
COORD
SUB
PERS
DEM
REF
REL
IMP
""".strip().split()
MAP = dict((x.split(":") for x in items.strip().split()))
def convert_morph(mf,pos):
   to_tag = [f for f in mf if f in MOVE_TO_TAG]
   if to_tag:
      assert(len(to_tag)==1)
      pos = pos + "-" + to_tag[0]
   mf = [MAP[f] for f in mf if (f not in DROP and f not in MOVE_TO_TAG)]
   return [x for x in sorted("|".join(mf).split("|")) if x],pos

if __name__ == '__main__':
   for sent in io.conll_to_sents(sys.stdin):
      for tok in sent:
         tok['morph'],tok['tag'] = convert_morph(tok['morph'],tok['tag'])
      if not tok['morph']: tok['morph'] = ['_']
      io.out_conll(sent)
