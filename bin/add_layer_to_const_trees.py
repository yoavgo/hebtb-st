import sys
import hebtb_reader
import codecs

def depreader(fname):
   out = []
   for line in file(fname):
      line = line.strip().split()
      if not line:
         if out: yield out
         out = []
      else:
         out.append(line)
   if out: yield out

for (i,ctree),dtree in zip(hebtb_reader.read_one_tree_per_line_as_stream(["(%s)" % x.strip().replace(", ",",") for x in codecs.open(sys.argv[1],encoding="utf8")]), depreader(sys.argv[2])):
   leafs = list(ctree.collect_leaves())
   assert(len(leafs)==len(dtree))
   for leaf,dtok in zip(leafs,dtree):
      pos,func,feats = leaf.get_pos().split("-")
      assert(pos == "DUMMY_POS")
      assert(func in ["[hd]","[compound]"]), func
      assert(feats == "[]")
      dfeats = dtok[-5]
      if dfeats == '_': dfeats = ''
      dpos = dtok[4]
      new_pos = "%s-%s-[[%s]]" % (dpos,func,dfeats)
      leaf.set_pos(new_pos)
      if not leaf.parent.name.startswith("SYN_"):
         leaf.parent.set_name("SYN_%s" % leaf.parent.name)
   print ctree.as_sexpr()

