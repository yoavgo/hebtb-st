from pio import io
import os.path
HERE = os.path.dirname(__file__)
import sys

FMAP = {}
for line in file(HERE + "/tb_morph_table"):
   tb,dtb = line.strip().split()
   FMAP[tb] = dtb

for sent in io.conll_to_sents(sys.stdin):
   for tok in sent:
      if tok['morph'][0][0] == '!':
         tok['morph'] = ["_"]
         tbfeats = None if "-" not in tok['tbtag'] else tok['tbtag'].split("-",1)[1]
         if tbfeats:
            tbfeats = tbfeats.split("-")
            if len(tbfeats) == 3:
               assert(tbfeats[1] == 'H')
               # Do the NN_S_PP case
               assert(tok['tag'] in ['NN','CD']),tok['form']
               tok['tag'] = "NN_S_PP"
               feats, sfeats = tbfeats[0],tbfeats[2]
            elif len(tbfeats) == 2:
               if tbfeats[1] == 'H':
                  feats = tbfeats[0]
                  sfeats = "_"
               elif tbfeats[0] == 'MD':
                  feats = tbfeats[1]
                  sfeats = "_"
               else:
                  assert(False)
            elif len(tbfeats) == 1:
               if tbfeats[0] == "M": continue
               if tbfeats[0] == "MD": continue
               feats = tbfeats[0]
               sfeats = "_"
            else:
               assert(False)
            if feats == "BR1H": feats = "BR1"
            morph = FMAP["%s|%s" % (feats, sfeats)]
            tok['morph'] = morph.split("|")
            #print tok['tag'],tbfeats,tok['tbtag']
   io.out_conll(sent)
