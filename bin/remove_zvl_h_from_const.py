import sys
import hebtb_reader

for i,tree in hebtb_reader.read_one_tree_per_line_as_stream(sys.stdin):
   ls = list(tree.collect_leaves())
   for i,l in enumerate(ls):
      if l.get_pos() == 'ZVL' and l.get_word() == 'H' and ls[i-1].get_word() == 'H':
         #print l.as_sexpr()
         l.remove()
   print tree.as_sexpr()
