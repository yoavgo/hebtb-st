import sys
sent = []
col = 3 if 'pos' in sys.argv else 1
for line in sys.stdin:
   line = line.strip().split()
   if not line:
      print " ".join(sent)
      sent = []
      continue
   if col == 3:
      sent.append(line[col].split("-")[0].split("_")[0])
   else:
      sent.append(line[col])

