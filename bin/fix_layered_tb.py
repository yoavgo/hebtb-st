import sys
import hebtb_reader

for i,tree in hebtb_reader.read_one_tree_per_line_as_stream(sys.stdin):
   # fix AT under PP to EM/IN
   leafs = list(tree.collect_leaves(ignore_empties=True))
   for leaf in leafs:
      if leaf.get_word() == "AT":
         parent = leaf.parent
         if parent.parent.get_name().startswith("PP"):
            parent.set_name("IN-DEP_HEAD")
            leaf.set_word("EM")
            leaf.set_pos("IN")
   # H KL -> HKL/NN-MF-SP (if under same parent)
   for (l1,l2) in zip(leafs,leafs[1:]):
      if l1.get_word() == "H" and l2.get_word() == "KL":
         if l1.parent == l2.parent:
            l2.remove()
            l1.set_word("HKL")
            l1.set_pos("NN-MF-SP")
   # DUMMY_AT AT -> DUMMY_AT AWT
   for l1 in leafs:
      if l1.get_pos() == "DUMMY_AT" and l1.get_word() == "AT":
         l1.set_word("AWT")
   print tree.as_sexpr()
