import sys
sys.path.append("bin")
from pio import io

def dep_lengths(fh):
   for sent in io.conll_to_sents(fh):
      yield " ".join([t['form'] for t in sent]), int(str(sent[-1]['id']).split(".")[0])

def raw_lengths(fh):
   for line in fh:
      yield line.strip(),len(line.strip().split())


for i,((r,lr),(d,ld)) in enumerate(zip(raw_lengths(file("new_data/treebanks/raw")),
                               dep_lengths(file("new_data/treebanks/deptb.unified_pos.wmorph.fixes.retoked")))):
   if lr != ld:
      print lr - ld
      print (lr,ld,r,d)


