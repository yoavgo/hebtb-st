import sys
sys.path.append("lattices/code")
import translate
for line in sys.stdin:
   line = line.decode("utf8").strip().split()
   line = [(translate.tb2heb(tok) if tok[0]!='y' else tok) for tok in line]
   print " ".join(line).encode("utf8")
